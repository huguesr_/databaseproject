# DatabaseProject
# STUDENTS: Hugues Rouillard 2242264, Vijay Patel 2136328 
# Gitlab Repo link:
https://gitlab.com/huguesr_/databaseproject.git

# ERD
https://lucid.app/lucidchart/8ebf11f4-cd8e-4240-832a-44c81c372b96/edit?viewport_loc=-445%2C186%2C2512%2C1234%2C0_0&invitationId=inv_717edd74-8810-48a9-a6af-dcc52a43f178
*File Design.pdf is also in project folder*

# TO SETUP DATABASE:
1. Run builder.sql
2. Run DataInsert.sql
3. Run AuditTables.sql
4. Run Procedures.sql (Compile package head before AND seperately from package body)
5. Run Triggers.sql (Run/compile every single trigger INDIVIDUALLY)
6. Run views.sql (Run/compile every view FIRST and BEFORE compiling package head and body seperately)

# TO REMOVE DATABASE:
Run remove.sql

# TO RUN JAVA APPLICATION:
Run App.java
