package db;
import java.sql.*;

public class Warehouse_Products {
    //This class is used to store records from the Project_Warehouse_Products table
    private int warehouseID;
    private int productID;
    private int quantity;

    public Warehouse_Products(int warehouseID, int productID, int quantity) {
        this.warehouseID = warehouseID;
        this.productID = productID;
        this.quantity = quantity;
    }
    //Getters and Setters
    public int getWarehouseID() {
        return this.warehouseID;
    }

    public void setWarehouseID(int warehouseID) {
        this.warehouseID = warehouseID;
    }

    public int getProductID() {
        return this.productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    //Adds a Warehouse_Products object to the DB
    public void addInventory(DBconnection dbConnection) throws SQLException {
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.addInventory(?,?,?)}")){
            statement.setInt(1,this.warehouseID);
            statement.setInt(2, this.productID);
            statement.setInt(3, this.quantity);
        statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }
    //Updates a row in the DB through a Warehouse_Products object with the new fields
    public void updateInventory(DBconnection dbConnection) throws SQLException {
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.updateInventory(?,?,?)}")){
            statement.setInt(1,this.warehouseID);
            statement.setInt(2, this.productID);
            statement.setInt(3, this.quantity);
        statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }
    //Deletes a row in the DB corresponding to a Warehouse_Products object
        public void deleteInventory(DBconnection dbConnection) throws SQLException {
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.deleteInventory(?,?)}")){
            statement.setInt(1,this.warehouseID);
            statement.setInt(2, this.productID);
            statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }
}
