package db;
import java.sql.*;
import java.sql.Date;
import java.util.*;

public class App
//This class is the main class of the application, it contains the main, which is where the whole program operates
{
    public static void main( String[] args ) throws SQLException{
        System.out.println("Welcome to the super store where yesterday's stuff is today's stuff and today's stuff is tomorrows stuff");
        Scanner input = new Scanner(System.in);
        DBconnection conn = null;
        boolean loginLoop = false;
        while(!loginLoop){
            try{
                System.out.println("Please enter your Username");
                String user = input.nextLine();

                System.out.println("Please enter your password! Do not worry, it is confidential :)");
                String pass = new String(System.console().readPassword("Password: "));
                conn = new DBconnection(user,pass);
                loginLoop = true;
            }
            catch(SQLException e){
                System.out.println("Login failed, please retry");
            }
        }

        //Loop for the whole program. Will be used to ask the user multiple times to either A. Continue, B. Exit
        //Then ask them what you asked, aka, display info, modify the DB, display audit
        
        boolean storeLoop = false;
        while(!storeLoop){
            //subject to change
            System.out.println("Would you like to enter or leave the superStore?");
            System.out.println("1 - Enter\n2 - Leave");
            int enterOrLeave = input.nextInt();

            if(enterOrLeave == 2){
                break;
            }

            boolean choiceOfActionValidation = false;
            while (!choiceOfActionValidation){
                System.out.println("What would you like to do? (enter respective number)");
                System.out.println("(1 - Display information on products/customers, 2 - Modify the Database, 3 - Display the audit log, 4 - Return to Previous options)");
                int userOperationChoice = input.nextInt();
                if (userOperationChoice == 1){
                    System.out.println("Choose an option: ");
                    System.out.println("(1 - Display products of a category, 2 - Inventory for a product, 3 - Average rating of a product, 4 - Flagged Customers, 5 - Num of orders for a product, 6 - Return to Main options )");
                    userOperationChoice = input.nextInt();
                    if(userOperationChoice == 1){
                        System.out.println("Enter a category: ");
                        ArrayList<Products> categoryProducts = (ArrayList<Products>) conn.retrieveProducts(input.next());
                        for(int i =0; i<categoryProducts.size(); i++){
                            System.out.println(categoryProducts.get(i));

                        }
                    }
                    else if(userOperationChoice == 2){
                        System.out.println("Enter a Product ID: ");
                        System.out.println(conn.getProductInventory(input.nextInt()));
                    }
                    else if(userOperationChoice ==3){
                        System.out.println("Enter a Product ID: ");
                        System.out.println(conn.getProductAverageScore(input.nextInt()));
                    }
                    else if(userOperationChoice ==4){
                        ArrayList<Customer> flaggedCustomers = (ArrayList<Customer>) conn.retrieveFlaggedCustomers();
                        for (int i=0; i<flaggedCustomers.size(); i++){
                            System.out.println(flaggedCustomers.get(i));
                        }
                    }
                    else if(userOperationChoice ==5){
                        System.out.println("Enter a Product ID: ");
                        System.out.println(conn.getNumOrderProducts(input.nextInt()));
                    } else if(userOperationChoice == 6){
                        break;
                    }
                    else {
                        System.out.println("Invalid input, try again");
                    }
                    
                }
                else if (userOperationChoice == 2){
                    System.out.println("How would you like to modify? (enter respective number)");
                    System.out.println("(1 - Add, 2 - Update, 3 - Delete, 4 - Return to Main options)");
                    userOperationChoice = input.nextInt();
                    input.nextLine();
                    if(userOperationChoice == 1){
                        System.out.println("What would you like to add to the DB? (enter respective number)");
                        System.out.println("(1 - Customer, 2 - Products, 3 - Location, 4 - Order, 5 - Review, 6 - Store, 7 - WareHouse, 8 - Customer_Products_Order, 9 - Product_Review, 10 - Store_Products, 11 - WareHouse_Products, 12 - Return to Main options)");
                        userOperationChoice = input.nextInt();
                        input.nextLine();
                        if (userOperationChoice ==1){
                            System.out.println("Please enter field values for new Customer: ");
                            System.out.println("Firstname: ");
                            String firstname = input.next();
                            System.out.println("Lastname: ");
                            String lastname = input.next();
                            System.out.println("email: ");
                            String email = input.next();
                            System.out.println("LocationID: ");
                            int locationIDc = input.nextInt();
                            Customer newC  = new Customer(0, firstname, lastname, email, locationIDc);
                            newC.addCustomer(conn);
                        }
                        else if (userOperationChoice ==2){
                            System.out.println("Please enter field values for new Product: ");
                            System.out.println("Product name: ");
                            String pname = input.next();
                            System.out.println("Category: ");
                            String categ = input.next();
                            input.nextLine();
                            Products newP  = new Products(0, pname, categ);
                            newP.addProducts(conn);
                        }
                        else if (userOperationChoice ==3){
                            System.out.println("Please enter field values for new Location: ");
                            System.out.println("Adress: ");
                            String adress = input.nextLine();
                            Location newL  = new Location(0, adress);
                            newL.addAddress(conn);
                        }
                        else if (userOperationChoice ==4){
                            System.out.println("Please enter field values for new Order: ");
                            System.out.println("Date: (FORMAT: \"yyyy-mm-dd\")");
                            String stringdate = input.nextLine();
                            Date odate = Date.valueOf(stringdate);
                            System.out.println("StoreID: ");
                            int storeIDo = input.nextInt();
                            System.out.println("CustomerID: ");
                            int custIDo = input.nextInt();
                            Orders newO  = new Orders(0, odate, storeIDo, custIDo);
                            newO.addOrders(conn);
                        }
                        else if (userOperationChoice ==5){
                            System.out.println("Please enter field values for new Review: ");
                            System.out.println("Flag number: ");
                            int flags = input.nextInt();
                            input.nextLine();
                            System.out.println("Description: ");
                            String desc = input.nextLine();
                            System.out.println("Review Stars: ");
                            int stars = input.nextInt();
                            Review newR  = new Review(0, flags, desc, stars);
                            newR.addReview(conn);
                        }
                        else if (userOperationChoice ==6){
                            System.out.println("Please enter field values for new Store: ");
                            System.out.println("Name: ");
                            String sName = input.nextLine();
                            Store newS  = new Store(0, sName);
                            newS.addStore(conn);
                        }
                        else if (userOperationChoice ==7){
                            System.out.println("Please enter field values for new WareHouse: ");
                            System.out.println("Name: ");
                            String wName = input.nextLine();
                            System.out.println("Location: ");
                            int locID = input.nextInt();
                            WareHouse newL  = new WareHouse(0, wName, locID);
                            newL.addWarehouse(conn);
                        }
                        else if (userOperationChoice ==8){
                            System.out.println("Please enter field values for new Customer_Products_Order: ");
                            System.out.println("ProductID: ");
                            int pID = input.nextInt();
                            System.out.println("OrderID: ");
                            int oID = input.nextInt();
                            System.out.println("Quantity: ");
                            int quant = input.nextInt();
                            Customer_Products_Order newCPO  = new Customer_Products_Order(pID, oID, quant);
                            newCPO.addCustomer_Products_Order(conn);
                        }
                        else if (userOperationChoice ==9){
                            System.out.println("Please enter field values for new Product_Review: ");
                            System.out.println("CustomerID: ");
                            int cID = input.nextInt();
                            System.out.println("ProductID: ");
                            int pID = input.nextInt();
                            System.out.println("ReviewID: ");
                            int rID = input.nextInt();
                            Product_Review newPR  = new Product_Review(cID, pID, rID);
                            newPR.addProductReview(conn);
                        }
                        else if (userOperationChoice ==10){
                            System.out.println("Please enter field values for new Store_Products: ");
                            System.out.println("StoreID: ");
                            int sID = input.nextInt();
                            System.out.println("ProductID: ");
                            int pID = input.nextInt();
                            System.out.println("Price: ");
                            double price = input.nextDouble();
                            Store_Products newSP  = new Store_Products(sID, pID, price);
                            newSP.addStore_Products(conn);
                        }
                        else if (userOperationChoice ==11){
                            System.out.println("Please enter field values for new Warehouse_Products: ");
                            System.out.println("WarehouseID: ");
                            int wID = input.nextInt();
                            System.out.println("ProductID: ");
                            int pID = input.nextInt();
                            System.out.println("Quantity: ");
                            int quantity = input.nextInt();
                            Warehouse_Products newWP  = new Warehouse_Products(wID, pID, quantity);
                            newWP.addInventory(conn);
                        } else if (userOperationChoice == 12){
                            break;
                        }else {
                            System.out.println("Invalid input, try again");
                        }
                    }
                    else if(userOperationChoice == 2){
                        System.out.println("What would you like to Modify? (enter respective number)");
                        System.out.println("(1 - Customer, 2 - Products, 3 - Location, 4 - Order, 5 - Review, 6 - Store, 7 - WareHouse, 8 - Customer_Products_Order, 9 - Product_Review, 10 - Store_Products, 11 - WareHouse_Products, 12 - Return to Main options)");
                        userOperationChoice = input.nextInt();
                        input.nextLine();
                        if (userOperationChoice ==1){
                            System.out.println("Please enter field values for the Customer: ");
                            System.out.println("CustomerID: ");
                            int cID = input.nextInt();
                            System.out.println("Firstname: ");
                            String firstname = input.next();
                            System.out.println("Lastname: ");
                            String lastname = input.next();
                            System.out.println("email: ");
                            String email = input.next();
                            System.out.println("LocationID: ");
                            int locationIDc = input.nextInt();
                            Customer newC  = new Customer(cID, firstname, lastname, email, locationIDc);
                            newC.updateCustomer(conn);
                        }
                        else if (userOperationChoice ==2){
                            System.out.println("Please enter field values for the Product: ");
                            System.out.println("ProductID: ");
                            int pID = input.nextInt();
                            String pname = "";
                            System.out.println("Category: ");
                            String categ = input.next();
                            input.nextLine();
                            Products newP  = new Products(pID, pname, categ);
                            newP.updateProducts(conn);
                        }
                        else if (userOperationChoice ==3){
                            System.out.println("Please enter field values for the Location: ");
                            System.out.println("LocationID: ");
                            int lID = input.nextInt();
                            input.nextLine();
                            System.out.println("Adress: ");
                            String adress = input.nextLine();
                            Location newL  = new Location(lID, adress);
                            newL.updateAddress(conn);
                        }
                        else if (userOperationChoice ==4){
                            System.out.println("Please enter field values for the Order: ");
                            System.out.println("OrderID: ");
                            int oID = input.nextInt();
                            input.nextLine();
                            System.out.println("Date: (FORMAT: \"yyyy-mm-dd\")");
                            String stringdate = input.nextLine();
                            Date odate = Date.valueOf(stringdate);
                            System.out.println("StoreID: ");
                            int storeIDo = input.nextInt();
                            System.out.println("CustomerID: ");
                            int custIDo = input.nextInt();
                            Orders newO  = new Orders(oID, odate, storeIDo, custIDo);
                            newO.updateOrders(conn);
                        }
                        else if (userOperationChoice ==5){
                            System.out.println("Please enter field values for the Review: ");
                            System.out.println("ReviewID: ");
                            int rID = input.nextInt();
                            System.out.println("Flag number: ");
                            int flags = input.nextInt();
                            input.nextLine();
                            System.out.println("Description: ");
                            String desc = input.nextLine();
                            System.out.println("Review Stars: ");
                            int stars = input.nextInt();
                            Review newR  = new Review(rID, flags, desc, stars);
                            newR.updateReview(conn);
                        }
                        else if (userOperationChoice ==6){
                            System.out.println("Please enter field values for the Store: ");
                            System.out.println("StoreID: ");
                            int sID = input.nextInt();
                            input.nextLine();
                            System.out.println("Name: ");
                            String sName = input.nextLine();
                            Store newS  = new Store(sID, sName);
                            newS.updateStore(conn);
                        }
                        else if (userOperationChoice ==7){
                            System.out.println("Please enter field values for the WareHouse: ");
                            System.out.println("WarehouseID: ");
                            int wID = input.nextInt();
                            input.nextLine();
                            System.out.println("Name: ");
                            String wName = input.nextLine();
                            System.out.println("LocationID: ");
                            int locID = input.nextInt();
                            WareHouse newL  = new WareHouse(wID, wName, locID);
                            newL.updateWarehouseLocation(conn);
                            newL.updateWarehouseName(conn);
                        }
                        else if (userOperationChoice ==8){
                            System.out.println("Please enter field values for the Customer_Products_Order: (Match ProuctID and OrderID to modify quantity)");
                            System.out.println("ProductID: ");
                            int pID = input.nextInt();
                            System.out.println("OrderID: ");
                            int oID = input.nextInt();
                            System.out.println("Quantity: ");
                            int quant = input.nextInt();
                            Customer_Products_Order newCPO  = new Customer_Products_Order(pID, oID, quant);
                            newCPO.updateCustomer_Products_Order(conn);
                        }
                        else if (userOperationChoice ==9){
                            System.out.println("Please enter field values for the Product_Review: ");
                            System.out.println("CustomerID: ");
                            int cID = input.nextInt();
                            System.out.println("ProductID: ");
                            int pID = input.nextInt();
                            System.out.println("ReviewID: ");
                            int rID = input.nextInt();
                            Product_Review newPR  = new Product_Review(cID, pID, rID);
                            newPR.addProductReview(conn);
                        }
                        else if (userOperationChoice ==10){
                            System.out.println("Please enter field values for the Store_Products: (Match StoreID and ProductID to modify Price)");
                            System.out.println("StoreID: ");
                            int sID = input.nextInt();
                            System.out.println("ProductID: ");
                            int pID = input.nextInt();
                            System.out.println("Price: ");
                            double price = input.nextDouble();
                            Store_Products newSP  = new Store_Products(sID, pID, price);
                            newSP.updateStore_Products(conn);
                        }
                        else if (userOperationChoice ==11){
                            System.out.println("Please enter field values for the Warehouse_Products: (Match WarehouseID and ProductID to modify Quantity)");
                            System.out.println("WarehouseID: ");
                            int wID = input.nextInt();
                            System.out.println("ProductID: ");
                            int pID = input.nextInt();
                            System.out.println("Quantity: ");
                            int quantity = input.nextInt();
                            Warehouse_Products newWP  = new Warehouse_Products(wID, pID, quantity);
                            newWP.updateInventory(conn);
                        } else if (userOperationChoice == 12){
                            break;
                        } else {
                            System.out.println("Invalid input, try again");
                        }
                    }
                    else if(userOperationChoice == 3){
                        System.out.println("--REMEMBER! Deleting a record will also delete the records it is referenced in!!--");
                        if(userOperationChoice == 3){ // Delete
                            System.out.println("Which table would you like to delete from? (Enter respective number)");
                            System.out.println("(1 - Products, 2 - Warehouse, 3 - Warehouse_Products, 4 - Order, 5 - Review, 6 - Return to Main Options");
                            userOperationChoice = input.nextInt();
                            input.nextLine();
                            if (userOperationChoice ==1){
                                System.out.println("Please enter the Product ID you'd like to purge! (This might take a while due to slow connection) \nID: ");
                                int pID = input.nextInt();
                                Products newP  = new Products(pID,"", "");
                                newP.deleteProduct(conn);
                            }
                            else if (userOperationChoice ==2){
                                System.out.println("Please enter the Warehouse ID you'd like to purge!\nID: ");
                                int wID = input.nextInt();
                                WareHouse newL  = new WareHouse(wID, "", 0);
                                newL.deleteWarehouse(conn);
                            }
                            else if (userOperationChoice ==3){
                                System.out.println("Please enter the Warehouse and Product ID you'd like to purge!");
                                System.out.println("WarehouseID: ");
                                int wID = input.nextInt();
                                System.out.println("ProductID: ");
                                int pID = input.nextInt();
                                Warehouse_Products newWP  = new Warehouse_Products(wID, pID, 0);
                                newWP.deleteInventory(conn);
                            }
                            else if (userOperationChoice ==4){
                                System.out.println("Please enter the Order ID you'd like to purge!");
                                System.out.println("OrderID: ");
                                int oID = input.nextInt();
                                Orders newO  = new Orders(oID);
                                newO.deleteOrders(conn);
                            }
                            else if (userOperationChoice ==5){
                                System.out.println("Please enter the Review ID you'd like to purge!");
                                System.out.println("ReviewID: ");
                                int rID = input.nextInt();
                                Review newPR  = new Review(rID, 0, "", 0);
                                newPR.deleteReview(conn);
                            } else if (userOperationChoice == 6){
                                break;
                            }
                            else {
                                System.out.println("Invalid input, try again!");
                            }
                        }
                    } else if (userOperationChoice == 4){
                        break;
                    }
                    else {
                        System.out.println("Invalid input, try again!");
                    }
                }
                else if (userOperationChoice == 3){
                    System.out.println("\nPrinting Audit logs! \n");
                    System.out.println("\nHere's the Location Logs\n");
                    System.out.println("----------------------------------------------");
                    conn.getAuditLocation();
                    System.out.println("----------------------------------------------");
                    System.out.println("\nHere's the Warehouse Logs\n");
                    System.out.println("----------------------------------------------");
                    conn.getAuditWarehouse();
                    System.out.println("----------------------------------------------");
                    System.out.println("\nHere's the Products Logs\n");
                    System.out.println("----------------------------------------------");
                    conn.getAuditProductsView();
                    System.out.println("----------------------------------------------");
                    System.out.println("\nHere's the Warehouse Products Logs\n");
                    System.out.println("----------------------------------------------");
                    conn.getAuditWarehouseProductsView();
                    System.out.println("----------------------------------------------");
                    System.out.println("\nHere's the Store logs\n");
                    System.out.println("----------------------------------------------");
                    conn.getAuditStoreView();
                    System.out.println("----------------------------------------------");
                    System.out.println("\nHere's the Store Product Logs\n");
                    System.out.println("----------------------------------------------");
                    conn.getAuditStoreProductView();
                    System.out.println("----------------------------------------------");
                    System.out.println("\nHere's the Review Logs\n");
                    System.out.println("----------------------------------------------");
                    conn.getAuditReviewView();
                    System.out.println("----------------------------------------------");
                    System.out.println("\nHere's the Customer Logs\n");
                    System.out.println("----------------------------------------------");
                    conn.getAuditCustomerView();
                    System.out.println("----------------------------------------------");
                    System.out.println("\nHere's the Product Review Logs\n");
                    System.out.println("----------------------------------------------");
                    conn.getAuditProductReviewView();
                    System.out.println("----------------------------------------------");
                    System.out.println("\nHere's the Orders Logs\n");
                    System.out.println("----------------------------------------------");
                    conn.getAuditOrdersView();
                    System.out.println("----------------------------------------------");
                    System.out.println("\nHere's the Customer Product Logs\n");
                    System.out.println("----------------------------------------------");
                    conn.getAuditCustomerProductsView();
                    System.out.println("----------------------------------------------");

                } else if (userOperationChoice == 4){
                    break;
                }
                else {
                    System.out.println("Invalid input, try again");
                }
            }
        }

        input.close();


        //Close the connection to the database
        conn.close();
        System.out.println("Goodbye");
    }
}
