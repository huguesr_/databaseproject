package db;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.OracleTypes;
import oracle.net.jdbc.TNSAddress.Address;
public class DBconnection {
    //This class is there to store the Connection to the DB, as well as contain methods needing an access to connection
    private Connection conn;

    public DBconnection(String user, String pass) throws SQLException{
            String url = "jdbc:oracle:thin:@198.168.52.211: 1521/pdbora19c.dawsoncollege.qc.ca";
            conn = DriverManager.getConnection(url, user, pass);
    }

    public void close(){
        try{
            if(this.conn != null){
                conn.close();
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
    }
    //Getter
    public Connection getConnection(){
        return this.conn;
    }
    //Calls getProductsByCategory and retrieves the products into Products objects
    public List<Products> retrieveProducts(String category) throws SQLException{
        try (CallableStatement statement = conn.prepareCall("{ call store_management.getProductsByCategory(?,?)}")){
            statement.setString(1, category);
            statement.registerOutParameter(2, OracleTypes.CURSOR);
            statement.execute(); 
            ResultSet resultSet = (ResultSet) statement.getObject(2);
            List<Products> productList = new ArrayList<Products>();

            while(resultSet.next()){
                int productID = resultSet.getInt("ProductID");
                String productName = resultSet.getString("ProductName");

                productList.add(new Products(productID, productName, category));
            }
            return productList;
        }
        catch (SQLException e){
            e.printStackTrace();
            return null;
        }

    }
    //Calls getProducAverageScore and returns the products average score
    public int getProductAverageScore(int productID) throws SQLException {
        try(CallableStatement statement = conn.prepareCall("{ ? = call store_management.getProductAverageScore(?)}")){
            statement.registerOutParameter(1, Types.INTEGER);
            statement.setInt(2, productID);
            statement.execute();
            int avgScore = statement.getInt(1);
            return avgScore;
        }
    }
    //Calls getProductInventory and returns the products total inventory
    public int getProductInventory(int productID) throws SQLException {
        try(CallableStatement statement = conn.prepareCall("{ ? = call store_management.getProductInventory(?)}")){
            statement.registerOutParameter(1, Types.INTEGER);
            statement.setInt(2, productID);
            statement.execute();
            int inventory = statement.getInt(1);
            return inventory;
        }
    }
    //Calls getNumOrdersProduct and returns the total amoutn of orders for a product
    public int getNumOrderProducts(int productID) throws SQLException {
        try(CallableStatement statement = conn.prepareCall("{ ? = call store_management.getNumOrdersProduct(?)}")){
            statement.registerOutParameter(1, Types.INTEGER);
            statement.setInt(2, productID);
            statement.execute();
            int ordersNum = statement.getInt(1);
            return ordersNum;
        }
    }
    //Calls getFlaggedCustomers and retrieves the customers into Customer objects
    public List<Customer> retrieveFlaggedCustomers() throws SQLException{
        try (CallableStatement statement = conn.prepareCall("{ call store_management.getFlaggedCustomers(?)}")){
            statement.registerOutParameter(1, OracleTypes.CURSOR);
            statement.execute();
            ResultSet resultSet = (ResultSet) statement.getObject(1);
            List<Customer> customerList = new ArrayList<Customer>();

            while(resultSet.next()){
                int custID = resultSet.getInt("CustID");
                String firstName = resultSet.getString("FirstName");
                String lastName = resultSet.getString("LastName");
                String email = resultSet.getString("Email");
                int locationID = resultSet.getInt("LocationID");

                customerList.add(new Customer(custID, firstName, lastName, email, locationID));
            }
            return customerList;
        }
        catch (SQLException e){
            e.printStackTrace();
            return null;
        }

    }
    //Calls getAuditLocationView and prints the audit records
    public void getAuditLocation() throws SQLException {
        try(CallableStatement statement = conn.prepareCall("{call elViews.getAuditLocationView(?)}")){
            statement.registerOutParameter(1, OracleTypes.CURSOR);
            statement.execute();
            ResultSet resultSet = (ResultSet) statement.getObject(1);
            while(resultSet.next()){
                int Audit_LocationID = resultSet.getInt("Audit_LocationID");
                int LocationID = resultSet.getInt("LocationID");
                String Address = resultSet.getString("Address");
                String Old_Address = resultSet.getString("Old_Address");
                String Action = resultSet.getString("Action");
                String Timestmp = resultSet.getString("Timestmp");

                System.out.println("Audit LocationID: " + Audit_LocationID);
                System.out.println("\nLocationID: " + LocationID);
                System.out.println("\nAddress: " + Address);
                System.out.println("\nOld_Address: " + Old_Address);
                System.out.println("\nAction: " + Action);
                System.out.println("\nTimestmp" + Timestmp);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }
    //Calls getAuditWarehouseView and prints the audit records
    public void getAuditWarehouse() throws SQLException {
        try(CallableStatement statement = conn.prepareCall("{call elViews.getAuditWarehouseView(?)}")){
            statement.registerOutParameter(1, OracleTypes.CURSOR);
            statement.execute();
            ResultSet resultSet = (ResultSet) statement.getObject(1);
            while(resultSet.next()){
                int Audit_WarehouseID = resultSet.getInt("Audit_WarehouseID");
                int WareHouseID = resultSet.getInt("WareHouseID");
                String Warehousename = resultSet.getString("Warehousename");
                String Old_Warehousename = resultSet.getString("Old_Warehousename");
                int LocationID = resultSet.getInt("LocationID");
                String Action = resultSet.getString("Action");
                String Timestmp = resultSet.getString("Timestmp");

                System.out.println("Audit_WarehouseID: " + Audit_WarehouseID);
                System.out.println("\nWareHouseID: " + WareHouseID);
                System.out.println("\nWarehousename: " + Warehousename);
                System.out.println("\nOld_Address: " + Old_Warehousename);
                System.out.println("\nLocationID: " + LocationID);
                System.out.println("\nAction: " + Action);
                System.out.println("\nTimestmp" + Timestmp);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }
    //Calls getAuditProductsView and prints the audit records
    public void getAuditProductsView() throws SQLException {
        try(CallableStatement statement = conn.prepareCall("{call elViews.getAuditProductsView(?)}")){
            statement.registerOutParameter(1, OracleTypes.CURSOR);
            statement.execute();
            ResultSet resultSet = (ResultSet) statement.getObject(1);
            while(resultSet.next()){
                int Audit_ProductID = resultSet.getInt("Audit_ProductID");
                int ProductID = resultSet.getInt("ProductID");
                String ProductName = resultSet.getString("ProductName");
                String Old_ProductName = resultSet.getString("Old_ProductName");
                String Category = resultSet.getString("Category");
                String Old_Category = resultSet.getString("Old_Category");
                String Action = resultSet.getString("Action");
                String Timestmp = resultSet.getString("Timestmp");

                System.out.println("Audit_ProductID: " + Audit_ProductID);
                System.out.println("\nProductID: " + ProductID);
                System.out.println("\nProductName: " + ProductName);
                System.out.println("\nOld_ProductName: " + Old_ProductName);
                System.out.println("\nCategory: " + Category);
                System.out.println("\nOld_Category: " + Old_Category);
                System.out.println("\nAction: " + Action);
                System.out.println("\nTimestmp" + Timestmp);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }
    //Calls getAuditWarehouseProductsView and prints the audit records
    public void getAuditWarehouseProductsView() throws SQLException {
        try(CallableStatement statement = conn.prepareCall("{call elViews.getAuditWarehouseProductsView(?)}")){
            statement.registerOutParameter(1, OracleTypes.CURSOR);
            statement.execute();
            ResultSet resultSet = (ResultSet) statement.getObject(1);
            while(resultSet.next()){
                int WarehouseID = resultSet.getInt("WarehouseID");
                int ProductID = resultSet.getInt("ProductID");
                int Quantity = resultSet.getInt("Quantity");
                int Old_Quantity = resultSet.getInt("Old_Quantity");
                String Action = resultSet.getString("Action");
                String Timestmp = resultSet.getString("Timestmp");

                System.out.println("WarehouseID: " + WarehouseID);
                System.out.println("\nProductID: " + ProductID);
                System.out.println("\nQuantity: " + Quantity);
                System.out.println("\nOld_Quantity: " + Old_Quantity);
                System.out.println("\nAction: " + Action);
                System.out.println("\nTimestmp" + Timestmp);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }
    //Calls getAuditStoreView and prints the audit records
    public void getAuditStoreView() throws SQLException {
        try(CallableStatement statement = conn.prepareCall("{call elViews.getAuditStoreView(?)}")){
            statement.registerOutParameter(1, OracleTypes.CURSOR);
            statement.execute();
            ResultSet resultSet = (ResultSet) statement.getObject(1);
            while(resultSet.next()){
                int Audit_StoreID = resultSet.getInt("Audit_StoreID");
                int StoreID = resultSet.getInt("StoreID");
                String StoreName = resultSet.getString("StoreName");
                String OldStoreName = resultSet.getString("OldStoreName");
                String Action = resultSet.getString("Action");
                String Timestmp = resultSet.getString("Timestmp");

                System.out.println("Audit_StoreID: " + Audit_StoreID);
                System.out.println("\nStoreID: " + StoreID);
                System.out.println("\nStoreName: " + StoreName);
                System.out.println("\nOldStoreName: " + OldStoreName);
                System.out.println("\nAction: " + Action);
                System.out.println("\nTimestmp" + Timestmp);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }
    //Calls getAuditStoreProductView and prints the audit records
    public void getAuditStoreProductView() throws SQLException {
        try(CallableStatement statement = conn.prepareCall("{call elViews.getAuditStoreProductsView(?)}")){
            statement.registerOutParameter(1, OracleTypes.CURSOR);
            statement.execute();
            ResultSet resultSet = (ResultSet) statement.getObject(1);
            while(resultSet.next()){
                int StoreID = resultSet.getInt("StoreID");
                int ProductID = resultSet.getInt("ProductID");
                int Price = resultSet.getInt("Price");
                int Old_Price = resultSet.getInt("Old_Price");
                String Action = resultSet.getString("Action");
                String Timestmp = resultSet.getString("Timestmp");

                System.out.println("StoreID: " + StoreID);
                System.out.println("\nProductID: " + ProductID);
                System.out.println("\nPrice: " + Price);
                System.out.println("\nOld_Price: " + Old_Price);
                System.out.println("\nAction: " + Action);
                System.out.println("\nTimestmp" + Timestmp);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }
    //Calls getAuditReviewView and prints the audit records
    public void getAuditReviewView() throws SQLException {
        try(CallableStatement statement = conn.prepareCall("{call elViews.getAuditReviewView(?)}")){
            statement.registerOutParameter(1, OracleTypes.CURSOR);
            statement.execute();
            ResultSet resultSet = (ResultSet) statement.getObject(1);
            while(resultSet.next()){
                int Audit_ReviewID = resultSet.getInt("Audit_ReviewID");
                int ReviewID = resultSet.getInt("ReviewID");
                int Flag = resultSet.getInt("Flag");
                int Old_Flag = resultSet.getInt("Old_Flag");
                String Description = resultSet.getString("Description");
                String Old_Description = resultSet.getString("Old_Description");
                String Action = resultSet.getString("Action");
                String Timestmp = resultSet.getString("Timestmp");

                System.out.println("Audit_ReviewID: " + Audit_ReviewID);
                System.out.println("\nReviewID: " + ReviewID);
                System.out.println("\nFlag: " + Flag);
                System.out.println("\nOld_Flag: " + Old_Flag);
                System.out.println("\nDescription: " + Description);
                System.out.println("\nOld_Description" + Old_Description);
                System.out.println("\nAction: " + Action);
                System.out.println("\nTimestmp" + Timestmp);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }
    //Calls getAuditCustomerView and prints the audit records
    public void getAuditCustomerView() throws SQLException {
        try(CallableStatement statement = conn.prepareCall("{call elViews.getAuditCustomerView(?)}")){
            statement.registerOutParameter(1, OracleTypes.CURSOR);
            statement.execute();
            ResultSet resultSet = (ResultSet) statement.getObject(1);
            while(resultSet.next()){
                int Audit_CustID = resultSet.getInt("Audit_CustID");
                int CustID = resultSet.getInt("CustID");
                String FirstName = resultSet.getString("FirstName");
                String Old_FirstName = resultSet.getString("Old_FirstName");
                String LastName = resultSet.getString("LastName");
                String Old_LastName = resultSet.getString("Old_LastName");
                String Email = resultSet.getString("Email");
                String Old_Email = resultSet.getString("Old_Email");
                String Action = resultSet.getString("Action");
                String Timestmp = resultSet.getString("Timestmp");

                System.out.println("Audit_CustID: " + Audit_CustID);
                System.out.println("\nCustID: " + CustID);
                System.out.println("\nFirstName: " + FirstName);
                System.out.println("\nOld_FirstName: " + Old_FirstName);
                System.out.println("\nLastName: " + LastName);
                System.out.println("\nOld_LastName" + Old_LastName);
                System.out.println("\nEmail: " + Email);
                System.out.println("\nOld_Email" + Old_Email);
                System.out.println("\nAction: " + Action);
                System.out.println("\nTimestmp" + Timestmp);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }
    //Calls getAuditProductReviewView and prints the audit records
    public void getAuditProductReviewView() throws SQLException {
        try(CallableStatement statement = conn.prepareCall("{call elViews.getAuditProductReviewView(?)}")){
            statement.registerOutParameter(1, OracleTypes.CURSOR);
            statement.execute();
            ResultSet resultSet = (ResultSet) statement.getObject(1);
            while(resultSet.next()){
                int CustID = resultSet.getInt("CustID");
                int ProductID = resultSet.getInt("ProductID");
                int ReviewID = resultSet.getInt("ReviewID");
                String Action = resultSet.getString("Action");
                String Timestmp = resultSet.getString("Timestmp");

                System.out.println("CustID: " + CustID);
                System.out.println("\nProductID: " + ProductID);
                System.out.println("\nReviewID: " + ReviewID);
                System.out.println("\nAction: " + Action);
                System.out.println("\nTimestmp" + Timestmp);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }  
    //Calls getAuditOrdersView and prints the audit records
    public void getAuditOrdersView() throws SQLException {
        try(CallableStatement statement = conn.prepareCall("{call elViews.getAuditOrdersView(?)}")){
            statement.registerOutParameter(1, OracleTypes.CURSOR);
            statement.execute();
            ResultSet resultSet = (ResultSet) statement.getObject(1);
            while(resultSet.next()){
                int Audit_OrderID = resultSet.getInt("Audit_OrderID");
                int OrderID = resultSet.getInt("OrderID");
                Date OrderDate = resultSet.getDate("OrderDate");
                Date Old_OrderDate = resultSet.getDate("Old_OrderDate");
                int StoreID = resultSet.getInt("StoreID");
                int CustID = resultSet.getInt("CustID");
                String Action = resultSet.getString("Action");
                String Timestmp = resultSet.getString("Timestmp");

                System.out.println("Audit_OrderID: " + Audit_OrderID);
                System.out.println("\nOrderID: " + OrderID);
                System.out.println("\nOrderDate: " + OrderDate);
                System.out.println("\nOld_OrderDate: " + Old_OrderDate);
                System.out.println("\nStoreID: " + StoreID);
                System.out.println("\nCustID: " + CustID);
                System.out.println("\nAction: " + Action);
                System.out.println("\nTimestmp" + Timestmp);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }
    //Calls getAuditCustomerProductOrdersView and prints the audit records
    public void getAuditCustomerProductsView() throws SQLException {
        try(CallableStatement statement = conn.prepareCall("{call elViews.getAuditCustomerProductOrdersView(?)}")){
            statement.registerOutParameter(1, OracleTypes.CURSOR);
            statement.execute();
            ResultSet resultSet = (ResultSet) statement.getObject(1);
            while(resultSet.next()){
                int ProductID = resultSet.getInt("ProductID");
                int OrderID = resultSet.getInt("OrderID");
                int Quantity = resultSet.getInt("Quantity");
                int Old_Quantity = resultSet.getInt("Old_Quantity");
                String Action = resultSet.getString("Action");
                String Timestmp = resultSet.getString("Timestmp");

                System.out.println("ProductID: " + ProductID);
                System.out.println("\nOrderID: " + OrderID);
                System.out.println("\nQuantity: " + Quantity);
                System.out.println("\nOld_Quantity: " + Old_Quantity);
                System.out.println("\nAction: " + Action);
                System.out.println("\nTimestmp" + Timestmp);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }
}

