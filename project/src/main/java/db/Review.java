package db;
import java.sql.*;

public class Review {
    //This class is used to store records from the Project_Review table
    private int reviewID;
    private int flag;
    private int review;
    private String description;

    public Review(int reviewID, int flag, String description,int review) {
        this.reviewID = reviewID;
        this.flag = flag;
        this.description = description;
        this.review = review;
    }
    //Getters and Setters
    public int getReview() {
        return this.review;
    }

    public void setReview(int review) {
        this.review = review;
    }
    
    public int getReviewID() {
        return this.reviewID;
    }

    public void setReviewID(int reviewID) {
        this.reviewID = reviewID;
    }

    public int getFlag() {
        return this.flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    //Adds a Review object to the DB
    public void addReview(DBconnection dbConnection) throws SQLException {
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.addReview(?,?,?)}")){
            statement.setInt(1,this.review);
            statement.setInt(2, this.flag);
            statement.setString(3, this.description);
            statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }
    //Updates a row in the DB through a Review object with the new fields
    public void updateReview(DBconnection dbConnection) throws SQLException {
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.updateReview(?,?,?,?)}")){
            statement.setInt(1, this.reviewID);
            statement.setInt(2,this.review);
            statement.setInt(3, this.flag);
            statement.setString(4, this.description);
            statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }
    //Deletes a row in the DB corresponding to a Review object
    public void deleteReview(DBconnection dbConnection) throws SQLException {
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.deleteReview(?)}")){
            statement.setInt(1, this.reviewID);
            statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }
}
