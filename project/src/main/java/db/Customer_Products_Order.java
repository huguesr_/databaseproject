package db;
import java.sql.*;

public class Customer_Products_Order {
    //This class is used to store records from the Project_Customer_Products_Order table
    private int productID;
    private int orderID;
    private int quantity;

    public Customer_Products_Order(int productID, int orderID, int quantity) {
        this.productID = productID;
        this.orderID = orderID;
        this.quantity = quantity;
    }
    //Getters and Setters
    public int getProductID() {
        return this.productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getOrderID() {
        return this.orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    //Adds a Customer_Products_Order object to the DB
    public void addCustomer_Products_Order(DBconnection dbConnection) throws SQLException {
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.addCustomer_Products_Order(?,?,?)}")){
            statement.setInt(1, this.productID);
            statement.setInt(2, this.orderID);
            statement.setInt(3, this.quantity);
            statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }
    //Updates a row in the DB through a Customer_Products_Order object with the new fields
    public void updateCustomer_Products_Order(DBconnection dbConnection) throws SQLException {
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.updateCustomer_Products_Order(?,?,?)}")){
            statement.setInt(1, this.productID);
            statement.setInt(2, this.orderID);
            statement.setInt(3, this.quantity);
            statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }
    //Deletes a row in the DB corresponding to a Customer_Products_Order object
    public void deleteCustomer_Products_Order(DBconnection dbConnection) throws SQLException {
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.deleteCustomer_Products_Order(?,?)}")){
            statement.setInt(1, this.productID);
            statement.setInt(2, this.orderID);
            statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }

}
