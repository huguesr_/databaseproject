package db;
import java.sql.*;

public class Orders {
    //This class is used to store records from the Project_Orders table
    private int orderID;
    private Date orderDate;
    private int storeID;
    private int custID;

    public Orders(int orderID, Date orderDate, int storeID, int custID) {
        this.orderID = orderID;
        this.orderDate = orderDate;
        this.storeID = storeID;
        this.custID = custID;
    }
    public Orders(int orderID){
        this.orderID = orderID;
    }
    //Getters and Setters
    public int getOrderID() {
        return this.orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public Date getOrderDate() {
        return this.orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public int getStoreID() {
        return this.storeID;
    }

    public void setStoreID(int storeID) {
        this.storeID = storeID;
    }

    public int getCustID() {
        return this.custID;
    }

    public void setCustID(int custID) {
        this.custID = custID;
    }
    //Adds a Orders object to the DB
    public void addOrders(DBconnection dbConnection) throws SQLException {
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.addOrders(?,?,?)}")){
            statement.setDate(1, this.orderDate);
            statement.setInt(2, this.storeID);
            statement.setInt(3, this.custID);
            statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }
    //Updates a row in the DB through an Orders object with the new fields
    public void updateOrders(DBconnection dbConnection) throws SQLException {
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.updateOrders(?,?,?,?)}")){
            statement.setInt(1, this.orderID);
            statement.setDate(2, this.orderDate);
            statement.setInt(3, this.storeID);
            statement.setInt(4, this.custID);
            statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }
    //Deletes a row in the DB corresponding to an Orders object
    public void deleteOrders(DBconnection dbConnection) throws SQLException {
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.deleteOrders(?)}")){
            statement.setInt(1, this.orderID);
            statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }
}
