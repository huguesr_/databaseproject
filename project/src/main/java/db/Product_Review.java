package db;
import java.sql.*;

public class Product_Review {
    //This class is used to store records from the Project_Product_Review table
    private int custID;
    private int productID;
    private int reviewID;

    public Product_Review(int custID, int productID, int reviewID) {
        this.custID = custID;
        this.productID = productID;
        this.reviewID = reviewID;
    }
    //Getters and Setters
    public int getCustID() {
        return this.custID;
    }

    public void setCustID(int custID) {
        this.custID = custID;
    }

    public int getProductID() {
        return this.productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getReviewID() {
        return this.reviewID;
    }

    public void setReviewID(int reviewID) {
        this.reviewID = reviewID;
    }
    //Adds a Product_Review object to the DB
    public void addProductReview(DBconnection dbConnection) throws SQLException {
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.addProductReview(?,?,?)}")){
            statement.setInt(1, this.custID);
            statement.setInt(2, this.productID);
            statement.setInt(3, this.reviewID);
            statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }
    //Deletes a row in the DB corresponding to a Product_Review object
    public void deleteProductReview(DBconnection dbConnection) throws SQLException {
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.deleteProductReview(?,?,?)}")){
            statement.setInt(1, this.custID);
            statement.setInt(2, this.productID);
            statement.setInt(3, this.reviewID);
            statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }
}
