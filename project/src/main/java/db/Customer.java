package db;
import java.sql.*;

public class Customer {
    //This class is used to store records from the Project_Customer table
    private int custID;
    private String firstName;
    private String lastName;
    private String email;
    private int locationID;

    public Customer(int custID, String firstName, String lastName, String email, int locationID) {
        this.custID = custID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.locationID = locationID;
    }
    //Getters and Setters
    public int getCustID() {
        return this.custID;
    }

    public void setCustID(int custID) {
        this.custID = custID;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getLocationID() {
        return this.locationID;
    }

    public void setLocationID(int locationID) {
        this.locationID = locationID;
    }
    //Adds a Customer object to the DB
    public void addCustomer(DBconnection dbConnection) throws SQLException {
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.addCustomer(?,?,?,?)}")){
            statement.setString(1, this.firstName);
            statement.setString(2, this.lastName);
            statement.setString(3, this.email);
            statement.setInt(4, this.locationID);
            statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }
    //Updates a row in the DB through a Customer object with the new fields
    public void updateCustomer(DBconnection dbConnection) throws SQLException {
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.updateCustomer(?,?,?,?,?)}")){
            statement.setInt(1, this.custID);
            statement.setString(2, this.firstName);
            statement.setString(3, this.lastName);
            statement.setString(4, this.email);
            statement.setInt(5, this.locationID);
            statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }
    //Deletes a row in the DB corresponding to a Customer object
    public void deleteCustomer(DBconnection dbConnection) throws SQLException {
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.deleteCustomer(?)}")){
            statement.setInt(1, this.custID);
            statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }

    public String toString(){
        String toReturn = "Customer ID: "+this.custID+"\n Full Name: "+this.firstName+" "+this.lastName+"\nEmail: "+this.email+"\nLocation ID: "+this.locationID+"\n-----";
        return toReturn;
    }
}
