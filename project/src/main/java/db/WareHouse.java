package db;
import java.sql.*;
public class WareHouse {
    //This class is used to store records from the Project_Warehouse table
    private int warehouseID;
    private String warehouseName;
    private int locationID;

    public WareHouse(int warehouseID, String warehouseName, int locationID) {
        this.warehouseID = warehouseID;
        this.warehouseName = warehouseName;
        this.locationID = locationID;
    }
    //Getters and Setters
    public int getWarehouseID() {
        return this.warehouseID;
    }

    public void setWarehouseID(int warehouseID) {
        this.warehouseID = warehouseID;
    }

    public String getWarehouseName() {
        return this.warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public int getLocationID() {
        return this.locationID;
    }

    public void setLocationID(int locationID) {
        this.locationID = locationID;
    }
    //Adds a Warehoouse object to the DB
    public void addWarehouse(DBconnection dbConnection) throws SQLException {
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.addWarehouse(?,?)}")){
            statement.setString(1,this.warehouseName);
            statement.setInt(2, this.locationID);
            statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }
    //Updates a row in the DB through a Warehouse object with the new field
    public void updateWarehouseName(DBconnection dbConnection) throws SQLException {
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.updateWarehouseName(?, ?)}")){
            statement.setInt(1, this.warehouseID);
            statement.setString(2, this.warehouseName);
            statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }
    //Updates a row in the DB through a Warehouse object with the new field
    public void updateWarehouseLocation(DBconnection dbConnection) throws SQLException {
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.updateWarehouseLocation(?, ?)}")){
            statement.setInt(1, this.warehouseID);
            statement.setInt(2, this.locationID);
            statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }
    //Deletes a row in the DB corresponding to a Warehouse object
    public void deleteWarehouse(DBconnection dbConnection) throws SQLException {
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.deleteWarehouse(?)}")){
            statement.setInt(1, this.warehouseID);
            statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }

}
