package db;
import java.sql.*;
public class Products {
    //This class is used to store records from the Project_Products table
    private int productID;
    private String productName;
    private String category;

    public Products(int productID, String productName, String category) {
        this.productID = productID;
        this.productName = productName;
        this.category = category;
    }
    //Getters and Setters
    public int getProductID() {
        return this.productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public String getProductName() {
        return this.productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    //Adds a Products object to the DB
    public void addProducts(DBconnection dbConnection) throws SQLException {
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.addProducts(?,?)}")){
            statement.setString(1,this.productName);
            statement.setString(2, this.category);
            statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }
    //Updates a row in the DB through a Products object with the new fields
    public void updateProducts(DBconnection dbConnection) throws SQLException {
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.updateProducts(?, ?)}")){
            statement.setInt(1, this.productID);
            statement.setString(2, this.category);
            statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }
    //Deletes a row in the DB corresponding to a Products object
        public void deleteProduct(DBconnection dbConnection) throws SQLException {
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.deleteProduct(?)}")){
            statement.setInt(1, this.productID);
            statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }
    //Prints correct format for Products
    public String toString(){
        String toReturn = "Product: "+this.productName+"\nProduct ID: "+this.productID+"\nCategory: "+this.category+"\n-----";
        return toReturn;
    }
}
