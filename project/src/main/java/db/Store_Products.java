package db;
import java.sql.*;

public class Store_Products {
    //This class is used to store records from the Project_Store_Products table
    private int storeID;
    private int productID;
    private double price;

    public Store_Products(int storeID, int productID, double price) {
        this.storeID = storeID;
        this.productID = productID;
        this.price = price;
    }
    //Getters and Setters
    public int getStoreID() {
        return this.storeID;
    }

    public void setStoreID(int storeID) {
        this.storeID = storeID;
    }

    public int getProductID() {
        return this.productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    //Adds a Store_Products object to the DB
    public void addStore_Products(DBconnection dbConnection) throws SQLException {
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.addStore_Products(?,?,?)}")){
            statement.setInt(1,this.storeID);
            statement.setInt(2, this.productID);
            statement.setDouble(3, this.price);
            statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }
    //Updates a row in the DB through a Store_Products object with the new fields
    public void updateStore_Products(DBconnection dbConnection) throws SQLException {
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.updateStore_Products(?,?,?)}")){
            statement.setInt(1,this.storeID);
            statement.setInt(2, this.productID);
            statement.setDouble(3, this.price);
            statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }
    //Deletes a row in the DB corresponding to a Store_Products object
    public void deleteStore_Products(DBconnection dbConnection) throws SQLException {
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.deleteStore_Products(?,?)}")){
            statement.setInt(1,this.storeID);
            statement.setInt(2, this.productID);
            statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }


}
