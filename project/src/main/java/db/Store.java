package db;
import java.sql.*;

public class Store {
    //This class is used to store records from the Project_Store table
    private int storeID;
    private String storeName;

    public Store(int storeID, String storeName) {
        this.storeID = storeID;
        this.storeName = storeName;
    }
    //Getters and Setters
    public int getStoreID() {
        return this.storeID;
    }

    public void setStoreID(int storeID) {
        this.storeID = storeID;
    }

    public String getStoreName() {
        return this.storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }
    //Adds a Store object to the DB
    public void addStore(DBconnection dbConnection) throws SQLException {
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.addStore(?)}")){
            statement.setString(1,this.storeName);
            statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }
    //Updates a row in the DB through a Store object with the new fields
    public void updateStore(DBconnection dbConnection) throws SQLException {
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.updateStore(?,?)}")){
            statement.setInt(1,this.storeID);
            statement.setString(2, this.storeName);
            statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }
    //Deletes a row in the DB corresponding to a Store object
    public void deleteStore(DBconnection dbConnection) throws SQLException {
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.deleteStore(?)}")){
            statement.setInt(1,this.storeID);
            statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }
}
