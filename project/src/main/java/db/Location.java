package db;

import java.sql.*;

public class Location {
    //This class is used to store records from the Project_Location table
    private int locationID;
    private String address;

    public Location(int locationID, String address) {
        this.locationID = locationID;
        this.address = address;
    }
    //Getters and Setters
    public int getLocationID() {
        return this.locationID;
    }

    public void setLocationID(int locationID) {
        this.locationID = locationID;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    //Adds a Location object to the DB
    public void addAddress(DBconnection dbConnection) throws SQLException {
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.addAddress(?)}")){
            statement.setString(1, this.address);
            statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }
    //Updates a row in the DB through a Location object with the new fields
    public void updateAddress(DBconnection dbConnection) throws SQLException{
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.updateAddress(?, ?)}")){
            statement.setInt(1, this.locationID);
            statement.setString(2, this.address);
            statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }
    //Deletes a row in the DB corresponding to a Location object
    public void deleteAddress(DBconnection dbConnection) throws SQLException{
        try(CallableStatement statement = dbConnection.getConnection().prepareCall("{call store_management.deleteAddress(?)}")){
            statement.setInt(1, this.locationID);
            statement.execute();
        } catch (SQLException e ){
            e.printStackTrace();
        }
    }
}
