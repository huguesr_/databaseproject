CREATE OR REPLACE VIEW Audit_Location_View AS
SELECT *
FROM Audit_Location;

CREATE OR REPLACE VIEW Audit_Warehouse_View AS
SELECT *
FROM Audit_Warehouse;

CREATE OR REPLACE VIEW Audit_Products_View AS
SELECT *
FROM Audit_Products;

CREATE OR REPLACE VIEW Audit_Warehouse_Products_View AS
SELECT *
FROM Audit_Warehouse_Products;

CREATE OR REPLACE VIEW Audit_Store_View AS
SELECT *
FROM Audit_Store;

CREATE OR REPLACE VIEW Audit_Store_Products_View AS
SELECT *
FROM Audit_Store_Products;

CREATE OR REPLACE VIEW Audit_Review_View AS
SELECT *
FROM Audit_Review;

CREATE OR REPLACE VIEW Audit_Customer_View AS
SELECT *
FROM Audit_Customer;

CREATE OR REPLACE VIEW Audit_Product_Review_View AS
SELECT *
FROM Audit_Product_Review;

CREATE OR REPLACE VIEW Audit_Orders_View AS
SELECT *
FROM Audit_Orders;

CREATE OR REPLACE VIEW Audit_Customer_Products_Order_View AS
SELECT *
FROM Audit_Customer_Products_Order;

/
CREATE OR REPLACE PACKAGE elViews AS
    PROCEDURE getAuditLocationView(location_cursor OUT SYS_REFCURSOR);
    PROCEDURE getAuditWarehouseView(WareHouse_cursor OUT SYS_REFCURSOR);
    PROCEDURE getAuditProductsView(Product_cursor OUT SYS_REFCURSOR);
    PROCEDURE getAuditWarehouseProductsView(Warehouse_Product_cursor OUT SYS_REFCURSOR);
    PROCEDURE getAuditStoreView(Store_cursor OUT SYS_REFCURSOR);
    PROCEDURE getAuditStoreProductsView(Store_Products_cursor OUT SYS_REFCURSOR);
    PROCEDURE getAuditReviewView(Review_cursor OUT SYS_REFCURSOR);
    PROCEDURE getAuditCustomerView(Customer_cursor OUT SYS_REFCURSOR);
    PROCEDURE getAuditProductReviewView(Product_Review_cursor OUT SYS_REFCURSOR);
    PROCEDURE getAuditOrdersView(Orders_cursor OUT SYS_REFCURSOR);
    PROCEDURE getAuditCustomerProductOrdersView(Customer_Product_Orders_cursor OUT SYS_REFCURSOR);
END elViews;
/

CREATE OR REPLACE PACKAGE BODY elViews AS
        --getAuditLocationView Doesn't take input and Selects the Audit records through the Audit_Location_View into a cursor, which is an OUT parameter, and returns it to the calling java method
        PROCEDURE getAuditLocationView(location_cursor OUT SYS_REFCURSOR) IS
            BEGIN
                OPEN location_cursor FOR
                    SELECT * FROM Audit_Location_View;
            END getAuditLocationView;
        --getAuditWarehouseView Doesn't take input and Selects the Audit records through the Audit_Warehouse_View into a cursor, which is an OUT parameter, and returns it to the calling java method
        PROCEDURE getAuditWarehouseView(WareHouse_cursor OUT SYS_REFCURSOR) IS
            BEGIN
                OPEN WareHouse_cursor FOR
                    SELECT * FROM Audit_Warehouse_View;
            END getAuditWarehouseView;
        --getAuditProductsView Doesn't take input and Selects the Audit records through the Audit_Products_View into a cursor, which is an OUT parameter, and returns it to the calling java method
        PROCEDURE getAuditProductsView(Product_cursor OUT SYS_REFCURSOR) IS
            BEGIN
                OPEN Product_cursor FOR
                    SELECT * FROM Audit_Products_View;
            END getAuditProductsView;
        --getAuditWarehouseProductsView Doesn't take input and Selects the Audit records through the Audit_Warehouse_Products_View into a cursor, which is an OUT parameter, and returns it to the calling java method
        PROCEDURE getAuditWarehouseProductsView(Warehouse_Product_cursor OUT SYS_REFCURSOR) IS
            BEGIN
                OPEN Warehouse_Product_cursor FOR
                    SELECT * FROM Audit_Warehouse_Products_View;
            END getAuditWarehouseProductsView;
        --getAuditStoreView Doesn't take input and Selects the Audit records through the Audit_Store_View into a cursor, which is an OUT parameter, and returns it to the calling java method
        PROCEDURE getAuditStoreView(Store_cursor OUT SYS_REFCURSOR) IS
            BEGIN
                OPEN Store_cursor FOR
                    SELECT * FROM Audit_Store_View;
            END getAuditStoreView;
        --getAuditStoreProductsView Doesn't take input and Selects the Audit records through the Audit_Store_Products_View into a cursor, which is an OUT parameter, and returns it to the calling java method
        PROCEDURE getAuditStoreProductsView(Store_Products_cursor OUT SYS_REFCURSOR) IS
            BEGIN
                OPEN Store_Products_cursor FOR
                    SELECT * FROM Audit_Store_Products_View;
            END getAuditStoreProductsView;
        --getAuditReviewView Doesn't take input and Selects the Audit records through the Audit_Review_View into a cursor, which is an OUT parameter, and returns it to the calling java method
        PROCEDURE getAuditReviewView(Review_cursor OUT SYS_REFCURSOR) IS
            BEGIN
                OPEN Review_cursor FOR
                    SELECT * FROM Audit_Review_View;
            END getAuditReviewView;
        --getAuditCustomerView Doesn't take input and Selects the Audit records through the Audit_Customer_View into a cursor, which is an OUT parameter, and returns it to the calling java method
        PROCEDURE getAuditCustomerView(Customer_cursor OUT SYS_REFCURSOR) IS
            BEGIN
                OPEN Customer_cursor FOR
                    SELECT * FROM Audit_Customer_View;
            END getAuditCustomerView;
        --getAuditProductReviewView Doesn't take input and Selects the Audit records through the Audit_Product_Review_View into a cursor, which is an OUT parameter, and returns it to the calling java method
        PROCEDURE getAuditProductReviewView(Product_Review_cursor OUT SYS_REFCURSOR) IS
            BEGIN
                OPEN Product_Review_cursor FOR
                    SELECT * FROM Audit_Product_Review_View;
            END getAuditProductReviewView;
        --getAuditOrdersView Doesn't take input and Selects the Audit records through the Audit_Orders_View into a cursor, which is an OUT parameter, and returns it to the calling java method
        PROCEDURE getAuditOrdersView(Orders_cursor OUT SYS_REFCURSOR) IS
            BEGIN
                OPEN Orders_cursor FOR
                    SELECT * FROM Audit_Orders_View;
            END getAuditOrdersView;
        --getAuditCustomerProductOrdersView Doesn't take input and Selects the Audit records through the Audit_Customer_Products_Order_View into a cursor, which is an OUT parameter, and returns it to the calling java method
        PROCEDURE getAuditCustomerProductOrdersView(Customer_Product_Orders_cursor OUT SYS_REFCURSOR) IS
            BEGIN
                OPEN Customer_Product_Orders_cursor FOR
                    SELECT * FROM Audit_Customer_Products_Order_View;
            END getAuditCustomerProductOrdersView;
END elViews;