CREATE OR REPLACE TRIGGER Audit_Location_update
AFTER INSERT OR UPDATE OR DELETE
ON Project_Location
For EACH ROW
BEGIN
    IF INSERTING THEN
        INSERT INTO Audit_Location (LocationID, Address, Old_Address,Action)
        VALUES (:NEW.LocationID, :NEW.Address, NULL,'INSERTING');
    END IF;

    IF UPDATING THEN
        INSERT INTO Audit_Location (LocationID, Address, Old_Address,Action)
        VALUES (:NEW.LocationID, :NEW.Address, :OLD.Address,'UPDATING');
    END IF;

    IF DELETING THEN
        dbms_output.put_line('before');
        INSERT INTO Audit_Location (LocationID, Address, Old_Address,Action)
        VALUES (:OLD.LocationID, NULL, :OLD.Address,'DELETING');
    END IF;
END;
/
CREATE OR REPLACE TRIGGER Audit_Warehouse_update
AFTER INSERT OR UPDATE OR DELETE
ON Project_Warehouse
FOR EACH ROW
BEGIN
    IF INSERTING THEN
        INSERT INTO Audit_Warehouse (WareHouseID,Warehousename,Old_Warehousename,LocationID,Action)
        VALUES (:NEW.WareHouseID,:NEW.Warehousename,NULL,:NEW.LocationID,'INSERTING');
    END IF;
    
    IF UPDATING THEN
        INSERT INTO Audit_Warehouse (WareHouseID,Warehousename,Old_Warehousename,LocationID,Action)
        VALUES (:NEW.WareHouseID,:NEW.Warehousename,:OLD.Warehousename,:OLD.LocationID,'UPDATING');
    END IF;

    IF DELETING THEN
        dbms_output.put_line('before');
        INSERT INTO Audit_Warehouse (WareHouseID, Warehousename, Old_Warehousename,LocationID,Action)
        VALUES (:OLD.WareHouseID, NULL, :OLD.Warehousename,:OLD.LocationID,'DELETING');
        dbms_output.put_line('after');
    END IF;
END;
/
CREATE OR REPLACE TRIGGER audit_products_trigger
AFTER INSERT OR UPDATE OR DELETE
ON Project_Products
FOR EACH ROW
BEGIN
    IF INSERTING THEN
        INSERT INTO Audit_Products (ProductID,ProductName,Old_ProductName,Category,Old_Category,Action)
        VALUES (:NEW.ProductID,:NEW.ProductName,NULL,:NEW.Category,NULL,'INSERTING');
    END IF;
    
    IF UPDATING THEN
        INSERT INTO Audit_Products (ProductID,ProductName,Old_ProductName,Category,Old_Category,Action)
        VALUES (:OLD.ProductID, :NEW.ProductName, :OLD.ProductName,:NEW.Category,:OLD.Category,'UPDATING');
    END IF;

    IF DELETING THEN
        dbms_output.put_line('before');
        INSERT INTO Audit_Products (ProductID,ProductName,Old_ProductName,Category,Old_Category,Action)
        VALUES (:OLD.ProductID, NULL, :OLD.ProductName, NULL, :OLD.Category,'DELETING');
    END IF;
END;

/
CREATE OR REPLACE TRIGGER audit_warehouse_products_trigger
AFTER INSERT OR UPDATE OR DELETE
ON Project_Warehouse_Products
FOR EACH ROW
BEGIN
    IF INSERTING THEN
        INSERT INTO Audit_Warehouse_Products (WareHouseID,ProductID,Quantity,Old_Quantity,Action)
        VALUES (:NEW.WareHouseID,:NEW.ProductID,:NEW.Quantity,NULL,'INSERTING');
    END IF;
    IF UPDATING THEN
        INSERT INTO Audit_Warehouse_Products (WarehouseID, ProductID, Quantity, Old_Quantity,Action)
        VALUES (:OLD.WarehouseID, :OLD.ProductID, :NEW.Quantity, :OLD.Quantity,'UPDATING');
    END IF;

    IF DELETING THEN
        dbms_output.put_line('before');
        INSERT INTO Audit_Warehouse_Products (WarehouseID, ProductID, Quantity, Old_Quantity,Action)
        VALUES (:OLD.WarehouseID, :OLD.ProductID, NULL, :OLD.Quantity,'DELETING');
    END IF;
END;
/
CREATE OR REPLACE TRIGGER audit_store_trigger
AFTER INSERT OR UPDATE OR DELETE
ON Project_Store
FOR EACH ROW
BEGIN
    IF INSERTING THEN
        INSERT INTO Audit_Store (StoreID,StoreName,OldStoreName,Action)
        VALUES (:NEW.StoreID,:NEW.StoreName,NULL,'INSERTING');
    END IF;
    IF UPDATING('StoreName') THEN
        INSERT INTO Audit_Store (StoreID, StoreName, OldStoreName,Action)
        VALUES (:OLD.StoreID, :NEW.StoreName, :OLD.StoreName,'UPDATING NAME');
    END IF;

    IF DELETING THEN
        dbms_output.put_line('before');
        INSERT INTO Audit_Store (StoreID, StoreName, OldStoreName,Action)
        VALUES (:OLD.StoreID, NULL, :OLD.StoreName,'DELETING');
    END IF;
END;
/
CREATE OR REPLACE TRIGGER audit_store_products_trigger
AFTER INSERT OR UPDATE OR DELETE
ON Project_Store_Products
FOR EACH ROW
BEGIN
    IF INSERTING THEN
        INSERT INTO Audit_Store_Products (StoreID,ProductID,Price,Old_Price,Action)
        VALUES (:NEW.StoreID,:NEW.ProductID,:NEW.Price,NULL,'INSERTING');
    END IF;
    IF UPDATING THEN
        INSERT INTO Audit_Store_Products (StoreID, ProductID, Price, Old_Price,Action)
        VALUES (:OLD.StoreID, :OLD.ProductID, :NEW.Price, :OLD.Price,'UPDATING');
    END IF;

    IF DELETING THEN
        dbms_output.put_line('before');
        INSERT INTO Audit_Store_Products (StoreID, ProductID, Price,Old_Price,Action)
        VALUES (:OLD.StoreID, :OLD.ProductID, NULL, :OLD.Price,'DELETING');
    END IF;
END;
/
CREATE OR REPLACE TRIGGER audit_review_trigger
AFTER INSERT OR UPDATE OR DELETE
ON Project_Review
FOR EACH ROW
BEGIN
    IF INSERTING THEN
        INSERT INTO Audit_Review (ReviewID,Flag,Old_Flag,Description,Old_Description,Action)
        VALUES (:NEW.ReviewID,:NEW.Flag,NULL,:NEW.Description,NULL,'INSERTING');
    END IF;
    IF UPDATING('Flag') THEN
        INSERT INTO Audit_Review (ReviewID,Flag,Old_Flag,Description,Old_Description,Action)
        VALUES (:NEW.ReviewID, :NEW.Flag, :OLD.Flag, :OLD.Description, :OLD.Description,'UPDATING FLAG');
    END IF;

    IF UPDATING('Description') THEN
        INSERT INTO Audit_Review (ReviewID,Flag,Old_Flag,Description,Old_Description,Action)
        VALUES (:NEW.ReviewID, :OLD.Flag, :OLD.Flag, :NEW.Description, :OLD.Description,'UPDATING DESCRIPTION');
    END IF;

    IF DELETING THEN
        dbms_output.put_line('before');
        INSERT INTO Audit_Review (ReviewID,Flag,Old_Flag,Description,Old_Description,Action)
        VALUES (:OLD.ReviewID, NULL, :OLD.Flag, NULL, :OLD.Description,'DELETING');
    END IF;
END;
/
CREATE OR REPLACE TRIGGER audit_customer_trigger
AFTER INSERT OR UPDATE OR DELETE
ON Project_Customer
FOR EACH ROW
BEGIN
    IF INSERTING THEN
        INSERT INTO Audit_Customer (CustID, FirstName, Old_FirstName, LastName, Old_LastName, Email, Old_Email, LocationID,Action)
        VALUES (:NEW.CustID,:NEW.FirstName,NULL,:NEW.LastName,NULL,:NEW.Email,NULL,:NEW.LocationID,'INSERTING');
    END IF;
    IF UPDATING('FirstName') THEN
        INSERT INTO Audit_Customer (CustID, FirstName, Old_FirstName, LastName, Old_LastName, Email, Old_Email, LocationID,Action)
        VALUES (:NEW.CustID, :NEW.FirstName, :OLD.FirstName, :OLD.LastName, :OLD.LastName, :OLD.Email, :OLD.Email, :OLD.LocationID,'UPDATING FIRSTNAME');
    END IF;

    IF UPDATING('LastName') THEN
        INSERT INTO Audit_Customer (CustID, FirstName, Old_FirstName, LastName, Old_LastName, Email, Old_Email, LocationID,Action)
        VALUES (:NEW.CustID, :OLD.FirstName, :OLD.FirstName, :NEW.LastName, :OLD.LastName, :OLD.Email, :OLD.Email, :OLD.LocationID,'UPDATING LASTNAME');
    END IF;

    IF UPDATING('Email') THEN
        INSERT INTO Audit_Customer (CustID, FirstName, Old_FirstName, LastName, Old_LastName, Email, Old_Email, LocationID,Action)
        VALUES (:NEW.CustID, :OLD.FirstName, :OLD.FirstName, :OLD.LastName, :OLD.LastName, :NEW.Email, :OLD.Email, :OLD.LocationID,'UPDATING EMAIL');
    END IF;

    IF DELETING THEN
        dbms_output.put_line('before');
        INSERT INTO Audit_Customer (CustID, FirstName, Old_FirstName, LastName, Old_LastName, Email, Old_Email, LocationID,Action)
        VALUES (:OLD.CustID, NULL, :OLD.FirstName, NULL, :OLD.LastName, NULL, :OLD.Email, :OLD.LocationID,'DELETING');
    END IF;
END;
/
CREATE OR REPLACE TRIGGER audit_product_review_trigger
AFTER INSERT OR UPDATE OR DELETE
ON Project_Product_Review
FOR EACH ROW
BEGIN
    IF INSERTING THEN
        INSERT INTO Audit_Product_Review (CustID,ProductID,ReviewID,Action)
        VALUES (:NEW.CustID,:NEW.ProductID,:NEW.ReviewID,'INSERTING');
    END IF;

    IF UPDATING('CustID') THEN
        INSERT INTO Audit_Product_Review (CustID, ProductID, ReviewID,Action)
        VALUES (:NEW.CustID, :OLD.ProductID, :OLD.ReviewID,'UPDATING CUSTID');
    END IF;

    IF UPDATING('ProductID') THEN
        INSERT INTO Audit_Product_Review (CustID, ProductID, ReviewID,Action)
        VALUES (:OLD.CustID, :NEW.ProductID, :OLD.ReviewID,'UPDATING PRODUCTID');
    END IF;

    IF UPDATING('ReviewID') THEN
        INSERT INTO Audit_Product_Review (CustID, ProductID, ReviewID,Action)
        VALUES (:OLD.CustID, :OLD.ProductID, :NEW.ReviewID,'UPDATING REVIEWID');
    END IF;

    IF DELETING THEN
        dbms_output.put_line('before');
        INSERT INTO Audit_Product_Review (CustID, ProductID, ReviewID,Action)
        VALUES (:OLD.CustID, :OLD.ProductID, :OLD.ReviewID,'DELETING');
    END IF;
END;
/
CREATE OR REPLACE TRIGGER audit_orders_trigger
AFTER INSERT OR UPDATE OR DELETE
ON Project_Orders
FOR EACH ROW
BEGIN
    IF INSERTING THEN
        INSERT INTO Audit_Orders (OrderID, OrderDate, Old_OrderDate, StoreID, CustID,Action)
        VALUES (:NEW.OrderID,:NEW.OrderDate,NULL,:NEW.StoreID,:NEW.CustID,'INSERTING');
    END IF;

    IF UPDATING('OrderDate') THEN
        INSERT INTO Audit_Orders (OrderID, OrderDate, Old_OrderDate, StoreID, CustID,Action)
        VALUES (:OLD.OrderID, :NEW.OrderDate, :OLD.OrderDate, :OLD.StoreID, :OLD.CustID,'UPDATING DATE');
    END IF;

    IF UPDATING('StoreID') THEN
        INSERT INTO Audit_Orders (OrderID, OrderDate, Old_OrderDate, StoreID, CustID,Action)
        VALUES (:OLD.OrderID, :OLD.OrderDate, :OLD.OrderDate, :NEW.StoreID, :OLD.CustID,'UPDATING STOREID');
    END IF;

    IF UPDATING('CustID') THEN
        INSERT INTO Audit_Orders (OrderID, OrderDate, Old_OrderDate, StoreID, CustID,Action)
        VALUES (:OLD.OrderID, :OLD.OrderDate, :OLD.OrderDate, :OLD.StoreID, :NEW.CustID,'UPDATING CUSTID');
    END IF;

    IF DELETING THEN
        dbms_output.put_line('before');
        INSERT INTO Audit_Orders (OrderID, OrderDate, Old_OrderDate, StoreID, CustID,Action)
        VALUES (:OLD.OrderID, NULL, :OLD.OrderDate, :OLD.StoreID, :OLD.CustID,'DELETING');
    END IF;
END;
/
CREATE OR REPLACE TRIGGER audit_customer_products_order_trigger
AFTER INSERT OR UPDATE OR DELETE
ON Project_Customer_Products_Order
FOR EACH ROW
BEGIN
    IF INSERTING THEN
        INSERT INTO Audit_Customer_Products_Order (ProductID,OrderID,Quantity,Old_Quantity,Action)
        VALUES (:NEW.ProductID,:NEW.OrderID,:NEW.Quantity,NULL,'INSERTING');
    END IF;

    IF UPDATING('ProductID') THEN
        INSERT INTO Audit_Customer_Products_Order (ProductID, OrderID, Quantity,Old_Quantity,Action)
        VALUES (:NEW.ProductID, :OLD.OrderID, :OLD.Quantity,:NEW.Quantity,'UPDATING PRODUCTID');
    END IF;

    IF UPDATING('OrderID') THEN
        INSERT INTO Audit_Customer_Products_Order (ProductID, OrderID, Quantity,Old_Quantity,Action)
        VALUES (:OLD.ProductID, :NEW.OrderID, :OLD.Quantity,:OLD.Quantity,'UPDATING ORDERID');
    END IF;

    IF UPDATING('Quantity') THEN
        INSERT INTO Audit_Customer_Products_Order (ProductID, OrderID, Quantity,Old_Quantity,Action)
        VALUES (:OLD.ProductID, :OLD.OrderID, :NEW.Quantity,:OLD.Quantity,'UPDATING QUANTITY');
    END IF;

    IF DELETING THEN
        dbms_output.put_line('before');
        INSERT INTO Audit_Customer_Products_Order (ProductID, OrderID, Quantity,Old_Quantity,Action)
        VALUES (:OLD.ProductID, :OLD.OrderID,NULL,:OLD.Quantity,'DELETING');
    END IF;
END;
/







