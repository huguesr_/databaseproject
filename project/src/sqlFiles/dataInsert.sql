--1.Project_Customer Address
INSERT INTO Project_Location (Address) --ID 1
VALUES ('dawson college, montreal, qeuebe, canada');
INSERT INTO Project_Location (Address) --ID 2
VALUES ('090 boul saint laurent, montreal, quebec, canada');
INSERT INTO Project_Location (Address) --ID 3
VALUES ('brossard, quebec, canada');
INSERT INTO Project_Location (Address) --ID 4
VALUES ('100 atwater street, toronto, canada');
INSERT INTO Project_Location (Address) --ID 5
VALUES ('boul saint laurent, montreal, quebec, canada');
INSERT INTO Project_Location (Address) --ID 6
VALUES ('100 young street, toronto, canada');
INSERT INTO Project_Location (Address) --ID 7
VALUES ('100 boul saint laurent, montreal, quebec, canada');
INSERT INTO Project_Location (Address) --ID 8
VALUES ('calgary, alberta, canada');
INSERT INTO Project_Location (Address) --ID 9
VALUES ('104 gill street, toronto, canada');
INSERT INTO Project_Location (Address) --ID 10
VALUES ('105 young street, toronto, canada');
INSERT INTO Project_Location (Address) --ID 11
VALUES ('87 boul saint laurent, montreal, quebec, canada');
INSERT INTO Project_Location (Address) --ID 12
VALUES ('76 boul decathlon, laval, quebec, canada');
INSERT INTO Project_Location (Address) --ID 13
VALUES ('22222 happy street, laval, quebec, canada');


--1.WareHouses Address
INSERT INTO Project_Location (Address) --ID 14
VALUES ('100 rue William, saint laurent, Quebec, Canada');
INSERT INTO Project_Location (Address) --ID 15
VALUES ('304 Rue François-Perrault, Villera Saint-Michel, Montréal, QC');
INSERT INTO Project_Location (Address) --ID 16
VALUES ('86700 Weston Rd, Toronto, Canada');
INSERT INTO Project_Location (Address) --ID 17
VALUES ('170  Sideroad, Quebec City, Canada');
INSERT INTO Project_Location (Address) --ID 18
VALUES ('1231 Trudea road, Ottawa, Canada');
INSERT INTO Project_Location (Address) --ID 19
VALUES ('16  Whitlock Rd, Alberta, Canada');

INSERT INTO Project_Location (Address) --ID 20
VALUES ('homeless guy ');

--2.Warehouses
INSERT INTO Project_Warehouse (Warehousename,LocationID) --ID 1
VALUES ('Project_Warehouse A',14);
INSERT INTO Project_Warehouse (Warehousename,LocationID) --ID 2
VALUES ('Project_Warehouse B',15);
INSERT INTO Project_Warehouse (Warehousename,LocationID) --ID 3
VALUES ('Project_Warehouse C',16);
INSERT INTO Project_Warehouse (Warehousename,LocationID) --ID 4
VALUES ('Project_Warehouse D',17);
INSERT INTO Project_Warehouse (Warehousename,LocationID) --ID 5
VALUES ('Project_Warehouse E',18);
INSERT INTO Project_Warehouse (Warehousename,LocationID) --ID 6
VALUES ('Project_Warehouse F',19);

--3.Product
INSERT INTO Project_Products(ProductName,Category) --ID 1
VALUES ('laptop ASUS 104S','electronics');
INSERT INTO Project_Products(ProductName,Category) --ID 2
VALUES ('apple','Grocery');
INSERT INTO Project_Products(ProductName,Category) --ID 3
VALUES ('SIMS CD','Video Games');
INSERT INTO Project_Products(ProductName,Category) --ID 4
VALUES ('orange','Grocery');
INSERT INTO Project_Products(ProductName,Category) --ID 5
VALUES ('Barbie Movie','DVD');
INSERT INTO Project_Products(ProductName,Category) --ID 6
VALUES ('LOreal Normal Hair','Health');
INSERT INTO Project_Products(ProductName,Category) --ID 7
VALUES ('BMW iX Lego','Toys');
INSERT INTO Project_Products(ProductName,Category) --ID 8
VALUES ('BMW i6','Cars');
INSERT INTO Project_Products(ProductName,Category) --ID 9
VALUES ('Truck 500c','Vehicle');
INSERT INTO Project_Products(ProductName,Category) --ID 10
VALUES ('paper towel','Beauty');
INSERT INTO Project_Products(ProductName,Category) --ID 11
VALUES ('plum','Grocery');
INSERT INTO Project_Products(ProductName,Category) --ID 12
VALUES ('Lamborghini Lego','Toys');
INSERT INTO Project_Products(ProductName,Category) --ID 13
VALUES ('chicken','grocery');
INSERT INTO Project_Products(ProductName,Category) --ID 14
VALUES ('pasta','Grocery');
INSERT INTO Project_Products(ProductName,Category) --ID 15
VALUES ('PS5','electronics');
INSERT INTO Project_Products(ProductName,Category) --ID 16
VALUES ('tomato','Grocery');
INSERT INTO Project_Products(ProductName,Category) --ID 18
VALUES ('Train X745','Vehicule');

--4.WareHouses Product
INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
VALUES (1,1,1000);
INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
VALUES (1,7,10);
INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
VALUES (1,8,6);
INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
VALUES (1,14,2132);
INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
VALUES (1,16,352222);
INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
VALUES (2,2,24980);
INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
VALUES (2,16,39484);
INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
VALUES (3,3,103);
INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
VALUES (3,11,43242);
INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
VALUES (4,4,35405);
INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
VALUES (4,11,6579);
INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
VALUES (4,15,123);
INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
VALUES (5,5,40);
INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
VALUES (5,15,1000);
INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
VALUES (5,12,98765);
INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
VALUES (5,15,4543);
INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
VALUES (6,6,450);
INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
VALUES (6,10,3532);
INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
VALUES (6,13,43523);

--5.Stores
INSERT INTO Project_Store(StoreName) --ID 1
VALUES ('marche adonis');
INSERT INTO Project_Store(StoreName) --ID 2
VALUES ('marche atwater');
INSERT INTO Project_Store(StoreName) --ID 3
VALUES ('dawson Project_Store');
INSERT INTO Project_Store(StoreName) --ID 4
VALUES ('Project_Store magic');
INSERT INTO Project_Store(StoreName) --ID 5
VALUES ('movie Project_Store');
INSERT INTO Project_Store(StoreName) --ID 6
VALUES ('super rue champlain');
INSERT INTO Project_Store(StoreName) --ID 7
VALUES ('toy r us');
INSERT INTO Project_Store(StoreName) --ID 8
VALUES ('Dealer One');
INSERT INTO Project_Store(StoreName) --ID 9
VALUES ('dealer montreal');
INSERT INTO Project_Store(StoreName) --ID 10
VALUES ('movie start');
INSERT INTO Project_Store(StoreName) --ID 11
VALUES ('star Project_Store');

--6.Stores-Product
INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
VALUES (1,1,970);
INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
VALUES (2,2,10);
INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
VALUES (3,3,50);
INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
VALUES (4,4,2);
INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
VALUES (5,5,30);
INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
VALUES (6,6,10);
INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
VALUES (7,7,40);
INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
VALUES (8,8,50000);
INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
VALUES (9,9,856600);
INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
VALUES (10,10,50);
INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
VALUES (2,11,10);
INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
VALUES (6,6,30);
INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
VALUES (7,12,40);
INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
VALUES (7,12,80);
INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
VALUES (5,3,16);
INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
VALUES (7,5,45);
INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
VALUES (1,13,9.5);
INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
VALUES (2,14,13.5);
INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
VALUES (11,15,200);
INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
VALUES (7,7,38);
INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
VALUES (4,14,15);

--7.Reviews
INSERT INTO Project_Review (Review,Flag,Description) --ID 1
VALUES (4,0,'it was affordable.');
INSERT INTO Project_Review (Review,Flag,Description) --ID 2
VALUES (3,0,'quality was not good');
INSERT INTO Project_Review (Review,Flag,Description) --ID 3
VALUES (2,1,NULL);
INSERT INTO Project_Review (Review,Flag,Description) --ID 4
VALUES (5,0,'highly recommend');
INSERT INTO Project_Review (Review,Flag,Description) --ID 5
VALUES (1,0,NULL);
INSERT INTO Project_Review (Review,Flag,Description) --ID 6
VALUES (1,0,'did not worth the price');
INSERT INTO Project_Review (Review,Flag,Description) --ID 7
VALUES (1,0,'missing some parts');
INSERT INTO Project_Review (Review,Flag,Description) --ID 8
VALUES (5,1,'trash');
INSERT INTO Project_Review (Review,Flag,Description) --ID 9
VALUES (2,0,NULL);
INSERT INTO Project_Review (Review,Flag,Description) --ID 10
VALUES (5,0,NULL);
INSERT INTO Project_Review (Review,Flag,Description) --ID 11
VALUES (4,0,NULL);
INSERT INTO Project_Review (Review,Flag,Description) --ID 12
VALUES (3,0,NULL);
INSERT INTO Project_Review (Review,Flag,Description) --ID 13 
VALUES (1,0,'missing some parts');
INSERT INTO Project_Review (Review,Flag,Description) --ID 14
VALUES (4,NULL,NULL);
INSERT INTO Project_Review (Review,Flag,Description) --ID 15 
VALUES (1,0,'great product');
INSERT INTO Project_Review (Review,Flag,Description) --ID 16 
VALUES (5,1,'bad quality');
INSERT INTO Project_Review (Review,Flag,Description) --ID 17 
VALUES (1,0,NULL);
INSERT INTO Project_Review (Review,Flag,Description) --ID 18 
VALUES (4,0,NULL);
INSERT INTO Project_Review (Review,Flag,Description) --ID 19
VALUES (4,NULL,NULL);
INSERT INTO Project_Review (Review,Flag,Description) --ID 20
VALUES (5,NULL,NULL);
INSERT INTO Project_Review (Review,Flag,Description) --ID 21
VALUES (1,2,'worse car i have droven!');
INSERT INTO Project_Review (Review,Flag,Description) --ID 22
VALUES (4,NULL,NULL);

--8.Customers
INSERT INTO Project_Customer (FirstName,LastName,Email,LocationID) --ID 1
VALUES ('mahsa','sadeghi','msadeghi@dawsoncollege.qc.ca',1);
INSERT INTO Project_Customer (FirstName,LastName,Email,LocationID) --ID 2
VALUES ('alex','brown','alex@gmail.com',2);
INSERT INTO Project_Customer (FirstName,LastName,Email,LocationID) --ID 3
VALUES ('martin','alexandre','marting@yahoo.com',3);
INSERT INTO Project_Customer (FirstName,LastName,Email,LocationID) --ID 4
VALUES ('daneil','hanne','daneil@yahoo.com',4);
INSERT INTO Project_Customer (FirstName,LastName,Email,LocationID) --ID 5
VALUES ('John','boura','bdoura@gmail.com',6);
INSERT INTO Project_Customer (FirstName,LastName,Email,LocationID) --ID 6
VALUES ('Ari','brown','b.a@gmail.com',20);
INSERT INTO Project_Customer (FirstName,LastName,Email,LocationID) --ID 7
VALUES ('Amanda','Harry','am.harry@yahioo.com',7);
INSERT INTO Project_Customer (FirstName,LastName,Email,LocationID) --ID 8
VALUES ('Jack','Jonhson','johnson.a@gmail.com',8);
INSERT INTO Project_Customer (FirstName,LastName,Email,LocationID) --ID 9
VALUES ('John','belle','abcd@yahoo.com',10);
INSERT INTO Project_Customer (FirstName,LastName,Email,LocationID) --ID 10
VALUES ('martin','Li','m.li@gmail.com',11);
INSERT INTO Project_Customer (FirstName,LastName,Email,LocationID) --ID 11
VALUES ('olivia','smith','smith@hotmail.com',12);
INSERT INTO Project_Customer (FirstName,LastName,Email,LocationID) --ID 12
VALUES ('Noah','Garcia','g.noah@yahoo.com',13);

--9.Product_Reviews
INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
VALUES (1,1,1);
INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
VALUES (2,2,2);
INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
VALUES (3,3,3);
INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
VALUES (4,4,4);
INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
VALUES (2,5,5);
INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
VALUES (3,6,6);
INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
VALUES (1,7,7);
INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
VALUES (5,8,8);
INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
VALUES (6,9,9);
INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
VALUES (7,10,10);
INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
VALUES (8,11,11);
INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
VALUES (3,6,12);
INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
VALUES (1,12,13);
INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
VALUES (1,11,14);
INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
VALUES (1,12,15);
INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
VALUES (9,8,16);
INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
VALUES (2,3,17);
INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
VALUES (2,5,18);
INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
VALUES (10,13,19);
INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
VALUES (11,14,20);
INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
VALUES (1,7,21);
INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
VALUES (12,14,22);

--10.Orders
INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 1
VALUES (TO_DATE('21/4/2023','dd/mm/yyyy'),1,1);
INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 2
VALUES (TO_DATE('23/10/2023','dd/mm/yyyy'),2,2);
INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 3
VALUES (TO_DATE('1/10/2023','dd/mm/yyyy'),3,3);
INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 4
VALUES (TO_DATE('23/10/2023','dd/mm/yyyy'),4,4);
INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 5
VALUES (TO_DATE('23/10/2023','dd/mm/yyyy'),5,2);
INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 6
VALUES (TO_DATE('10/10/2023','dd/mm/yyyy'),6,3);
INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 7
VALUES (TO_DATE('11/10/2023','dd/mm/yyyy'),7,1);
INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 8
VALUES (TO_DATE('10/10/2023','dd/mm/yyyy'),8,5);
INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 9
VALUES (TO_DATE('6/5/2020','dd/mm/yyyy'),2,8);
INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 10
VALUES (TO_DATE('12/9/2019','dd/mm/yyyy'),6,3);
INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 11
VALUES (TO_DATE('11/10/2010','dd/mm/yyyy'),7,1);
INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 12
VALUES (TO_DATE('6/5/2022','dd/mm/yyyy'),2,1);
INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 13
VALUES (TO_DATE('7/10/2023','dd/mm/yyyy'),7,1);
INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 14
VALUES (TO_DATE('10/8/2023','dd/mm/yyyy'),8,9);
INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 15
VALUES (TO_DATE('23/10/2023','dd/mm/yyyy'),5,2);
INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 16
VALUES (TO_DATE('2/10/2023','dd/mm/yyyy'),7,2);
INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 17
VALUES (TO_DATE('3/4/2019','dd/mm/yyyy'),1,10);
INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 18
VALUES (TO_DATE('29/12/2021','dd/mm/yyyy'),2,11);
INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 19
VALUES (TO_DATE('20/1/2020','dd/mm/yyyy'),11,12);
INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 20
VALUES (TO_DATE('11/10/2022','dd/mm/yyyy'),7,1);
INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 21
VALUES (TO_DATE('29/12/2021','dd/mm/yyyy'),4,11);

--11.Customer_Products_Order
INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
VALUES (1,1,1); --laptop
INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
VALUES (2,2,2); --apple
INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
VALUES (3,3,3); --SIMS
INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
VALUES (4,4,1); --orange
INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
VALUES (5,5,1); --Barbie
INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
VALUES (6,6,1); --Hair
INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
VALUES (7,7,1); --BMW Lego
INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
VALUES (8,8,1); --BMW car
INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
VALUES (11,9,6); --plum
INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
VALUES (6,10,3); --Hair
INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
VALUES (12,11,1); --Lambo Lego
INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
VALUES (11,12,7); --plum
INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
VALUES (12,13,2); --lambo lego
INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
VALUES (8,14,1); --BMW car
INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
VALUES (3,15,1); --Sims
INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
VALUES (5,16,1); --Barbie
INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
VALUES (13,17,1); --chicken
INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
VALUES (14,18,3); --Pasta
INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
VALUES (15,19,1); --PS5
INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
VALUES (7,20,1); --BMW lego
INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
VALUES (14,21,3); --pasta