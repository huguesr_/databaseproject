CREATE OR REPLACE PACKAGE store_management AS
    --1.Location
    PROCEDURE addAddress(v_address IN Project_Location.Address%TYPE);
    PROCEDURE updateAddress(in_LocationID IN Project_Location.LocationID%TYPE, new_address IN Project_Location.Address%TYPE);
    PROCEDURE deleteAddress(in_LocationID IN Project_Location.LocationID%TYPE);
    --2.WareHouse
    PROCEDURE addWarehouse(in_warehouseName IN Project_Warehouse.Warehousename%TYPE,in_LocationID IN Project_Warehouse.LocationID%TYPE);
    PROCEDURE updateWarehouseName(in_warehouseID IN Project_Warehouse.WareHouseID%TYPE,new_warehouseName IN Project_Warehouse.Warehousename%TYPE);
    PROCEDURE updateWarehouseLocation(in_warehouseID IN Project_Warehouse.WarehouseID%TYPE,new_locationID IN Project_Warehouse.LocationID%TYPE);
    PROCEDURE deleteWarehouse(in_warehouseID IN Project_Warehouse.WareHouseID%TYPE);
    --3.Products
    PROCEDURE addProducts(in_product_Name IN Project_Products.ProductName%TYPE ,in_product_Category Project_Products.Category%TYPE);
    PROCEDURE updateProducts(in_productID IN Project_Products.ProductID%TYPE,new_Category IN Project_Products.Category%TYPE);
    PROCEDURE deleteProduct(in_productID IN Project_Products.ProductID%TYPE);
    PROCEDURE getProductsByCategory(product_Category IN Project_Products.Category%TYPE, products_cursor OUT SYS_REFCURSOR);
    FUNCTION getProductAverageScore(product_ID IN Project_Products.ProductID%TYPE)RETURN NUMBER;
    FUNCTION getProductInventory(product_ID IN Project_Products.ProductID%TYPE)RETURN NUMBER;
    FUNCTION getNumOrdersProduct(product_ID IN Project_Products.ProductID%TYPE)RETURN NUMBER;
    --4.WareHouseProducts
    PROCEDURE addInventory(in_warehouseID IN Project_Warehouse_Products.WarehouseID%TYPE, in_productID IN Project_Warehouse_Products.ProductID%TYPE, in_quantity IN Project_Warehouse_Products.Quantity%TYPE);
    PROCEDURE updateInventory(in_warehouseID IN Project_Warehouse_Products.WareHouseID%TYPE, in_productID IN Project_Warehouse_Products.ProductID%TYPE, in_quantity IN Project_Warehouse_Products.Quantity%TYPE);
    PROCEDURE deleteInventory(in_warehouseID IN Project_Warehouse_Products.WarehouseID%TYPE, in_productID IN Project_Warehouse_Products.ProductID%TYPE);
    --5.Store
    PROCEDURE addStore(in_StoreName IN Project_Store.StoreName%TYPE);
    PROCEDURE updateStore(in_StoreID IN Project_Store.StoreID%TYPE, in_StoreName IN Project_Store.StoreName%TYPE);
    PROCEDURE deleteStore(in_StoreID IN Project_Store.StoreID%TYPE);
    --6.Store Products
    PROCEDURE addStore_Products(in_StoreID IN Project_Store_Products.StoreID%TYPE, in_productID IN Project_Store_Products.ProductID%TYPE,in_Price IN Project_Store_Products.price%TYPE);
    PROCEDURE updateStore_Products(in_StoreID IN Project_Store_Products.StoreID%TYPE, in_productID IN Project_Store_Products.ProductID%TYPE,in_Price IN Project_Store_Products.price%TYPE);
    PROCEDURE deleteStore_Products(in_StoreID IN Project_Store_Products.StoreID%TYPE, in_productID IN Project_Store_Products.ProductID%TYPE);
    --7.Review
    PROCEDURE addReview(in_Review IN Project_Review.Review%TYPE,in_flag IN Project_Review.Flag%TYPE,in_description IN Project_Review.Description%TYPE);
    PROCEDURE updateReview(in_ReviewID IN Project_Review.ReviewID%TYPE, in_Review IN Project_Review.Review%TYPE, in_flag IN Project_Review.Flag%TYPE,in_description IN Project_Review.Description%TYPE);
    PROCEDURE deleteReview(in_ReviewID IN Project_Review.ReviewID%TYPE);
    --8.Customers
    PROCEDURE addCustomer(in_fname IN Project_Customer.FirstName%TYPE,in_lname IN Project_Customer.LastName%TYPE, in_email IN Project_Customer.Email%TYPE, in_LocationID IN Project_Customer.LocationID%TYPE);
    PROCEDURE updateCustomer(in_custID IN Project_Customer.CustID%TYPE, in_fname IN Project_Customer.FirstName%TYPE,in_lname IN Project_Customer.LastName%TYPE, in_email IN Project_Customer.Email%TYPE, in_LocationID IN Project_Customer.LocationID%TYPE);
    PROCEDURE deleteCustomer(in_custID IN Project_Customer.CustID%TYPE);
    PROCEDURE getFlaggedCustomers(customers_cursor OUT SYS_REFCURSOR);
    --9.Product_Review
    PROCEDURE addProductReview(in_custID IN Project_Product_Review.CustID%TYPE, in_productID IN Project_Product_Review.ProductID%TYPE, in_ReviewID IN Project_Product_Review.ReviewID%TYPE);
    PROCEDURE updateProductReview(in_custID IN Project_Product_Review.CustID%TYPE, in_productID IN Project_Product_Review.ProductID%TYPE, in_ReviewID IN Project_Product_Review.ReviewID%TYPE);
    PROCEDURE deleteProductReview(in_custID IN Project_Product_Review.CustID%TYPE, in_productID IN Project_Product_Review.ProductID%TYPE, in_ReviewID IN Project_Product_Review.ReviewID%TYPE);
    --10.Orders
    PROCEDURE addOrders(in_OrderDate IN Project_Orders.OrderDate%TYPE, in_StoreID IN Project_Orders.StoreID%TYPE, in_custID IN Project_Orders.CustID%TYPE);
    PROCEDURE updateOrders(in_orderID IN Project_Orders.OrderID%TYPE,in_OrderDate IN Project_Orders.OrderDate%TYPE, in_StoreID IN Project_Orders.StoreID%TYPE, in_custID IN Project_Orders.CustID%TYPE);
    PROCEDURE deleteOrders(in_orderID IN Project_Orders.OrderID%TYPE);
    --11.Customer_Products_Order
    PROCEDURE addCustomer_Products_Order(in_ProductID IN Project_Customer_Products_Order.ProductID%TYPE, in_OrderID IN Project_Customer_Products_Order.OrderID%TYPE, in_Quantity IN Project_Customer_Products_Order.Quantity%TYPE);
    PROCEDURE updateCustomer_Products_Order(in_ProductID IN Project_Customer_Products_Order.ProductID%TYPE, in_OrderID IN Project_Customer_Products_Order.OrderID%TYPE, in_Quantity IN Project_Customer_Products_Order.Quantity%TYPE);
    PROCEDURE deleteCustomer_Products_Order(in_ProductID IN Project_Customer_Products_Order.ProductID%TYPE, in_OrderID IN Project_Customer_Products_Order.OrderID%TYPE);
END store_management;
/
CREATE OR REPLACE PACKAGE BODY store_management AS
    --1. Location
    --addAddress Takes as input a String of text representing an adress and Inserts into Project_Location a new Location having the inputted Address (PK generated automatically)
    PROCEDURE addAddress(v_address IN Project_Location.Address%TYPE) IS
    BEGIN
        INSERT INTO Project_Location (Address)
        VALUES (v_address);
    End addAddress;
    --updateAddress Takes as input a LocationID to modify, and a New address to replace the previous one and Updates the specified record in Project_Location
    PROCEDURE updateAddress(in_LocationID IN Project_Location.LocationID%TYPE, new_address IN Project_Location.Address%TYPE) IS
    BEGIN
        UPDATE Project_Location
        SET Address = new_address
        WHERE LocationID = in_LocationID;
    END updateAddress;
    --deleteAddress Takes as input a LocationID and deletes the corresponding row in Project_Location
    PROCEDURE deleteAddress(in_LocationID IN Project_Location.LocationID%TYPE) IS
    BEGIN
        DELETE FROM Project_Location
        WHERE LocationID = in_LocationID;
    END deleteAddress;

    --2. WareHouse
    --addWarehouse Takes as input a String of text representing the Warehouse's name, and a location ID and Inserts into Project_Warehouse a new Warehouse with the provided fields (PK generated automatically)
    PROCEDURE addWarehouse(in_warehouseName IN Project_Warehouse.Warehousename%TYPE,in_LocationID IN Project_Warehouse.LocationID%TYPE) IS
    BEGIN
        INSERT INTO Project_Warehouse(Warehousename,LocationID)
        VALUES(in_warehouseName,in_LocationID);
    END addWarehouse;
    --updateWarehouseName Takes as input a WarehouseID to modify, and a New Name to replace the previous one and Updates the specified record in Project_Warehouse
    PROCEDURE updateWarehouseName(in_warehouseID IN Project_Warehouse.WarehouseID%TYPE,new_warehouseName IN Project_Warehouse.Warehousename%TYPE) IS
    BEGIN
        UPDATE Project_Warehouse
        SET Warehousename = new_warehouseName
        WHERE WarehouseID = in_warehouseID;
    END updateWarehouseName;
    --updateWarehouseLocation Takes as input a WarehouseID to modify, and a New LocationID to replace the previous one and Updates the specified record in Project_Warehouse
    PROCEDURE updateWarehouseLocation(in_warehouseID IN Project_Warehouse.WarehouseID%TYPE,new_locationID IN Project_Warehouse.LocationID%TYPE) IS
    BEGIN
        UPDATE Project_Warehouse
        SET LocationID = new_locationID
        WHERE WarehouseID = in_warehouseID;
    END updateWarehouseLocation;
    --deleteWarehouse Takes as input a WarehouseID and deletes the corresponding row in Project_Warehouse
    PROCEDURE deleteWarehouse(in_warehouseID IN Project_Warehouse.WareHouseID%TYPE) IS
    BEGIN
        DELETE FROM Project_Warehouse_Products
        WHERE WarehouseID = in_warehouseID;

        DELETE FROM Project_Warehouse
        WHERE WarehouseID = in_warehouseID;
    END deleteWarehouse;

    --3.Products
    --addProducts Takes as input a String of text representing the Product's name, and a Category and Inserts into Project_Products a new Product with the provided fields (PK generated automatically)
    PROCEDURE addProducts(in_product_Name IN Project_Products.ProductName%TYPE ,in_product_Category Project_Products.Category%TYPE) IS
    BEGIN
        INSERT INTO Project_Products(ProductName,Category)
        VALUES(in_product_Name,in_product_Category);
    END addProducts;
    --updateProducts Takes as input a ProductID to modify, and a New Category to replace the previous one and Updates the specified record in Project_Products
    PROCEDURE updateProducts(in_productID IN Project_Products.ProductID%TYPE,new_Category IN Project_Products.Category%TYPE) IS
    BEGIN
        UPDATE Project_Products
        SET Category = new_Category
        Where ProductID = in_productID;
    END updateProducts;
    --deleteProduct Takes as input a ProductID and deletes the corresponding rows in Project_Customer_Products_Order, Project_Warehouse_Products, Project_Store_Products, Project_Product_Review and Project_Products (to remove child rows)
    PROCEDURE deleteProduct(in_productID IN Project_Products.ProductID%TYPE) IS
    BEGIN
        DELETE FROM Project_Customer_Products_Order
        WHERE ProductID = in_productID;

        DELETE FROM Project_Warehouse_Products
        WHERE ProductID = in_productID;

        DELETE FROM Project_Store_Products
        WHERE ProductID = in_productID;

        DELETE FROM Project_Product_Review
        WHERE ProductID = in_productID;

        DELETE FROM Project_Products
        WHERE ProductID = in_productID;
    END deleteProduct;
    --getProductsByCategory Takes a Category as input and Selects all the products under that category into a cursor, which is an out parameter and returned to the calling java method
    PROCEDURE getProductsByCategory(product_Category IN Project_Products.Category%TYPE, products_cursor OUT SYS_REFCURSOR) IS
    BEGIN
        OPEN products_cursor FOR
            SELECT ProductID, ProductName FROM Project_Products 
            WHERE Category = product_Category;
    END getProductsByCategory;
    --getProductAverageScore Takes a ProductID as input and Returns a number (the average score computed), it selects the average review score of every reviews concerning the product in question and returns it
    FUNCTION getProductAverageScore(product_ID IN Project_Products.ProductID%TYPE)RETURN NUMBER IS
        reviewAvg NUMBER;
        BEGIN
            SELECT AVG(r.Review) INTO reviewAvg FROM Project_Review r
            INNER JOIN Project_Product_Review pr USING (ReviewID)
            WHERE pr.ProductID = product_ID
            GROUP BY pr.ProductID;
            RETURN(reviewAvg);
        END getProductAverageScore;
    --getProductInventory Takes a ProductID as input and returns a Number (the total inventory of a product across all warehouses), it sums up the quantity of the specified product in all warehouses and returns it
    FUNCTION getProductInventory(product_ID IN Project_Products.ProductID%TYPE)RETURN NUMBER IS
        p_inventory NUMBER;
        BEGIN
            SELECT SUM(wp.Quantity) INTO p_inventory FROM Project_Warehouse_Products wp
            WHERE wp.ProductID = product_ID
            GROUP BY wp.ProductID;
            RETURN(p_inventory);
        END getProductInventory;
    --getNumOrdersProduct Takes a ProductID as input and returns a Number (the total amount of orders for a product), it counts every orders concerning the specified product and returns it
    FUNCTION getNumOrdersProduct(product_ID IN Project_Products.ProductID%TYPE)RETURN NUMBER IS
        p_numOrders NUMBER;
        BEGIN
	        SELECT COUNT(*) INTO p_numOrders FROM Project_Customer_Products_Order cpo
            WHERE cpo.ProductID = product_ID
            GROUP BY cpo.ProductID;
            RETURN(p_numOrders);
        END getNumOrdersProduct;
    --4.WarehouseProducts
    --addInventory Takes as input a WarehouseID, a ProductID and a Quantity, and Inserts into Project_Warehouse_Products a new Warehouse_Product with the provided fields, defines a quantity of a certain product in a warehouse(PK generated automatically)
    PROCEDURE addInventory(in_warehouseID IN Project_Warehouse_Products.WarehouseID%TYPE, in_productID IN Project_Warehouse_Products.ProductID%TYPE, in_quantity IN Project_Warehouse_Products.Quantity%TYPE) IS
    BEGIN
        INSERT INTO Project_Warehouse_Products
        VALUES (in_warehouseID,in_productID,in_quantity);
    END addInventory;
    --updateInventory Takes as input a WarehouseID and a ProductID to match the exact row to modify, and a New Quantity to replace the previous one and Updates the specified record in Project_Warehouse_Products
    PROCEDURE updateInventory(in_warehouseID IN Project_Warehouse_Products.WareHouseID%TYPE, in_productID IN Project_Warehouse_Products.ProductID%TYPE, in_quantity IN Project_Warehouse_Products.Quantity%TYPE) IS
    BEGIN
        UPDATE Project_Warehouse_Products
        SET Quantity = in_quantity
        WHERE ProductID = in_productID AND WareHouseID = in_warehouseID;
    END updateInventory;
    --deleteInventory Takes as input a WarehouseID and a ProductID and deletes the corresponding row in Project_Warehouse_Products
    PROCEDURE deleteInventory(in_warehouseID IN Project_Warehouse_Products.WarehouseID%TYPE, in_productID IN Project_Warehouse_Products.ProductID%TYPE) IS
    BEGIN
        DELETE FROM Project_Warehouse_Products
        WHERE ProductID = in_productID AND WareHouseID = in_warehouseID;
    END deleteInventory;

    --5.Store
    --addStore Takes as input a String of text representing the Store's name and Inserts into Project_Store a new Store with the provided fields (PK generated automatically)
    PROCEDURE addStore(in_StoreName IN Project_Store.StoreName%TYPE) IS
    BEGIN
        INSERT INTO Project_Store(StoreName)
        VALUES (in_StoreName);
    END addStore;
    --updateStore Takes as input a StoreID to modify, and a New Name to replace the previous one and Updates the specified record in Project_Store
    PROCEDURE updateStore(in_StoreID IN Project_Store.StoreID%TYPE, in_StoreName IN Project_Store.StoreName%TYPE) IS
    BEGIN
        UPDATE Project_Store
        SET StoreName = in_StoreName
        WHERE StoreID = in_StoreID;
    END updateStore;
    --deleteStore Takes as input a StoreID and deletes the corresponding row in Project_Store
    PROCEDURE deleteStore(in_StoreID IN Project_Store.StoreID%TYPE) IS
    BEGIN
        DELETE FROM Project_Store
        WHERE StoreID = in_StoreID;
    END deleteStore;

    --6.Store_Products
    --addStore_Products Takes as input a StoreID, a ProductID, and a Price, and Inserts into Project_Store_Products a new Store_Product with the provided fields, links a product with its price in a certain store (PK generated automatically)
    PROCEDURE addStore_Products(in_StoreID IN Project_Store_Products.StoreID%TYPE, in_productID IN Project_Store_Products.ProductID%TYPE,in_Price IN Project_Store_Products.price%TYPE) IS
    BEGIN
        INSERT INTO Project_Store_Products(StoreID,ProductID,Price)
        VALUES (in_StoreID,in_productID,in_Price);
    END addStore_Products;
    --updateStore_Products Takes as input a StoreID and a ProductID to match the exact row to modify, and a New Price to replace the previous one and Updates the specified record in Project_Store_Products
    PROCEDURE updateStore_Products(in_StoreID IN Project_Store_Products.StoreID%TYPE, in_productID IN Project_Store_Products.ProductID%TYPE,in_Price IN Project_Store_Products.price%TYPE) IS
    BEGIN
        UPDATE Project_Store_Products
        SET Price = in_Price
        WHERE StoreID = in_StoreID AND ProductID = in_productID;
    END updateStore_Products;
    --deleteStore_Products Takes as input a StoreID and a ProductID and deletes the corresponding row in Project_Store_Products
    PROCEDURE deleteStore_Products(in_StoreID IN Project_Store_Products.StoreID%TYPE, in_productID IN Project_Store_Products.ProductID%TYPE) IS
    BEGIN
        DELETE FROM Project_Store_Products
        WHERE StoreID = in_StoreID AND ProductID = in_productID;
    END deleteStore_Products;

    --7.Review
    --addReview Takes as input a Number "Review" that represents the rating given out of 5, the number of flags on the review, and a Description and Inserts into Project_Review a new Review with the provided fields (PK generated automatically)
    PROCEDURE addReview(in_Review IN Project_Review.Review%TYPE,in_flag IN Project_Review.Flag%TYPE,in_description IN Project_Review.Description%TYPE) IS
    BEGIN
        INSERT INTO Project_Review(Review,Flag,Description)
        VALUES (in_Review,in_flag,in_description);
    END addReview;
    --updateReview Takes as input a ReviewID to modify, a New Review score, a New Flag number and a New Description to replace the previous ones and Updates the specified record in Project_Review
    PROCEDURE updateReview(in_ReviewID IN Project_Review.ReviewID%TYPE, in_Review IN Project_Review.Review%TYPE, in_flag IN Project_Review.Flag%TYPE,in_description IN Project_Review.Description%TYPE) IS
    BEGIN
        UPDATE Project_Review
        SET Review = in_Review,
            Flag = in_flag,
            Description = in_description
        WHERE ReviewID = in_ReviewID;
    END updateReview;
    --deleteReview Takes as input a ReviewID and deletes the corresponding rows in Project_Product_Review and Project_Review (to remove child rows)
    PROCEDURE deleteReview(in_ReviewID IN Project_Review.ReviewID%TYPE) IS
    BEGIN
        DELETE FROM Project_Product_Review
        WHERE ReviewID = in_ReviewID;

        DELETE FROM Project_Review
        WHERE ReviewID = in_ReviewID;
    END deleteReview;

    --8.Customers
    --addCustomer Takes as input a first name, a last name, an email (all strings so far), and a LocationID and Inserts into Project_Customer a new Customer with the provided fields (PK generated automatically)
    PROCEDURE addCustomer(in_fname IN Project_Customer.FirstName%TYPE,in_lname IN Project_Customer.LastName%TYPE, in_email IN Project_Customer.Email%TYPE, in_LocationID IN Project_Customer.LocationID%TYPE) IS
    BEGIN
        INSERT INTO Project_Customer (FirstName,LastName,Email,LocationID)
        VALUES (in_fname,in_lname,in_email,in_LocationID);
    END addCustomer;
    --updateCustomer Takes as input a CustomerID to modify, a New First Name, a New Last Name, a New Email and a New LocationID to replace the previous ones and Updates the specified record in Project_Customer
    PROCEDURE updateCustomer(in_custID IN Project_Customer.CustID%TYPE, in_fname IN Project_Customer.FirstName%TYPE,in_lname IN Project_Customer.LastName%TYPE, in_email IN Project_Customer.Email%TYPE, in_LocationID IN Project_Customer.LocationID%TYPE) IS
    BEGIN
        UPDATE Project_Customer
        SET FirstName = in_fname,
            LastName = in_lname,
            Email = in_email,
            LocationID = in_LocationID
        WHERE CustID = in_custID;
    END updateCustomer;
    --deleteCustomer Takes as input a CustomerID and deletes the corresponding row in Project_Customer
    PROCEDURE deleteCustomer(in_custID IN Project_Customer.CustID%TYPE) IS
    BEGIN
        DELETE FROM Project_Customer
        WHERE CustID = in_custID;
    END deleteCustomer;
    --getFlaggedCustomers doesn't take any input and Selects all the customers who have a Review with 2 or more flags into a cursor, which is an out parameter and returned to the calling java method
    PROCEDURE getFlaggedCustomers(customers_cursor OUT SYS_REFCURSOR) IS
    BEGIN
        OPEN customers_cursor FOR
            SELECT c.CustID, c.FirstName, c.LastName, c.Email, c.LocationID FROM Project_Customer  c
            INNER JOIN Project_Product_Review pr ON pr.CustID = c.CustID
            INNER JOIN Project_Review r ON r.ReviewID = pr.ReviewID
            WHERE r.Flag >= 2;
    END getFlaggedCustomers;

    --9.Product Review
    --addProductReview Takes as input a CustomerID, a ProductID and a ReviewID and Inserts into Project_Product_Review a new Product_Review with the provided fields, linking a review to a certain product and the customer that wrote it (PK generated automatically)
    PROCEDURE addProductReview(in_custID IN Project_Product_Review.CustID%TYPE, in_productID IN Project_Product_Review.ProductID%TYPE, in_ReviewID IN Project_Product_Review.ReviewID%TYPE) IS
    BEGIN
        INSERT INTO Project_Product_Review (CustID,ProductID,ReviewID)
        VALUES(in_custID,in_productID,in_ReviewID);
    END addProductReview;
    --updateProductReview Takes as input a CustomerID and a ProductID to match the exact row to modify, and a New ReviewID to replace the previous one and Updates the specified record in Project_Product_Review
    PROCEDURE updateProductReview(in_custID IN Project_Product_Review.CustID%TYPE, in_productID IN Project_Product_Review.ProductID%TYPE, in_ReviewID IN Project_Product_Review.ReviewID%TYPE) IS
    BEGIN
        UPDATE Project_Product_Review SET 
        ReviewID = in_ReviewID
        WHERE CustID = in_custID AND ProductID = in_productID;
    END updateProductReview;
    --deleteProductReview Takes as input a CustomerID, a ProductID and a ReviewID and deletes the corresponding row in Project_Product_Review
    PROCEDURE deleteProductReview(in_custID IN Project_Product_Review.CustID%TYPE, in_productID IN Project_Product_Review.ProductID%TYPE, in_ReviewID IN Project_Product_Review.ReviewID%TYPE) IS
    BEGIN
        DELETE FROM Project_Product_Review
        WHERE CustID = in_custID AND ProductID = in_productID AND ReviewID = in_ReviewID;
    END deleteProductReview;

    --10.Orders
    --addOrders Takes as input an order Date, a StoreID and a CustomerID and Inserts into Project_Orders a new Order with the provided fields, linking a customer with the store they ordered from and the date of the order (PK generated automatically)
    PROCEDURE addOrders(in_OrderDate IN Project_Orders.OrderDate%TYPE, in_StoreID IN Project_Orders.StoreID%TYPE, in_custID IN Project_Orders.CustID%TYPE) IS
    BEGIN
        INSERT INTO Project_Orders (OrderDate,StoreID,CustID)
        VALUES(in_OrderDate,in_StoreID,in_custID);
    END addOrders;
    --updateOrders Takes as input an OrderID to modify, a New Order Date, a New StoreID and a New CustomerID to replace the previous ones and Updates the specified record in Project_Orders
    PROCEDURE updateOrders(in_orderID IN Project_Orders.OrderID%TYPE,in_OrderDate IN Project_Orders.OrderDate%TYPE, in_StoreID IN Project_Orders.StoreID%TYPE, in_custID IN Project_Orders.CustID%TYPE) IS
    BEGIN
        UPDATE Project_Orders
        SET OrderDate = in_OrderDate,
        StoreID = in_StoreID,
        CustID = in_custID
        WHERE OrderID = in_orderID;
    END updateOrders;
    --deleteOrders Takes as input an OrderID and deletes the corresponding rows in Project_Customer_Products_Order and Project_Orders (to remove child rows)
    PROCEDURE deleteOrders(in_orderID IN Project_Orders.OrderID%TYPE) IS
    BEGIN
        DELETE FROM Project_Customer_Products_Order
        WHERE OrderID = in_orderID;

        DELETE FROM Project_Orders
        WHERE OrderID = in_orderID;
    END deleteOrders;

    --11.Customer_Products_Order
    --addCustomer_Products_Order Takes as input a ProductID, an OrderID, and a Quantity and Inserts into Project_Customer_Products_Order a new Customer_Product_Order with the provided fields, linking a product to a certain order and the quantity ordered (PK generated automatically)
    PROCEDURE addCustomer_Products_Order(in_ProductID IN Project_Customer_Products_Order.ProductID%TYPE, in_OrderID IN Project_Customer_Products_Order.OrderID%TYPE, in_Quantity IN Project_Customer_Products_Order.Quantity%TYPE) IS
    BEGIN
        INSERT INTO Project_Customer_Products_Order (ProductID, OrderID, Quantity)
        VALUES (in_productID, in_orderID,in_Quantity);
    END addCustomer_Products_Order;
    --updateCustomer_Products_Order Takes as input a ProductID and a OrderID to match the exact row to modify, and a New Quantity to replace the previous one and Updates the specified record in Project_Customer_Products_Order
    PROCEDURE updateCustomer_Products_Order(in_ProductID IN Project_Customer_Products_Order.ProductID%TYPE, in_OrderID IN Project_Customer_Products_Order.OrderID%TYPE, in_Quantity IN Project_Customer_Products_Order.Quantity%TYPE) IS
    BEGIN
        UPDATE Project_Customer_Products_Order
        SET Quantity = in_Quantity
        WHERE ProductID = in_productID AND OrderID = in_OrderID;
    END updateCustomer_Products_Order;
    --deleteCustomer_Products_Order Takes as input a ProductID and an OrderID and deletes the corresponding row in Project_Customer_Products_Order
    PROCEDURE deleteCustomer_Products_Order(in_ProductID IN Project_Customer_Products_Order.ProductID%TYPE, in_OrderID IN Project_Customer_Products_Order.OrderID%TYPE) IS
    BEGIN
        DELETE FROM Project_Customer_Products_Order
        WHERE ProductID = in_productID AND OrderID = in_orderID;
    END deleteCustomer_Products_Order;

END store_management;