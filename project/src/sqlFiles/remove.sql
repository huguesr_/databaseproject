DROP TRIGGER audit_customer_products_order_trigger;
DROP TRIGGER audit_orders_trigger;
DROP TRIGGER audit_product_review_trigger;
DROP TRIGGER audit_customer_trigger;
DROP TRIGGER audit_review_trigger;
DROP TRIGGER audit_store_products_trigger;
DROP TRIGGER audit_store_trigger;
DROP TRIGGER audit_warehouse_products_trigger;
DROP TRIGGER audit_products_trigger;
DROP TRIGGER Audit_Warehouse_update;
DROP TRIGGER Audit_Location_update;

Drop PACKAGE store_management;

Drop Table Audit_Location;
Drop Table Audit_Warehouse;
Drop Table Audit_Products;
Drop Table Audit_Warehouse_Products;
Drop Table Audit_Store;
Drop Table Audit_Store_Products;
Drop Table Audit_Review;
Drop Table Audit_Customer;
Drop Table Audit_Product_Review;
Drop Table Audit_Orders;
Drop Table Audit_Customer_Products_Order;

DROP TABLE Project_Customer_Products_Order CASCADE CONSTRAINTS;
DROP TABLE Project_Orders CASCADE CONSTRAINTS;
DROP TABLE Project_Product_Review CASCADE CONSTRAINTS;
DROP TABLE Project_Customer CASCADE CONSTRAINTS;
DROP TABLE Project_Review CASCADE CONSTRAINTS;
DROP TABLE Project_Store_Products CASCADE CONSTRAINTS;
DROP TABLE Project_Store CASCADE CONSTRAINTS;
DROP TABLE Project_Warehouse_Products CASCADE CONSTRAINTS;
DROP TABLE Project_Products CASCADE CONSTRAINTS;
DROP TABLE Project_Warehouse CASCADE CONSTRAINTS;
DROP TABLE Project_Location CASCADE CONSTRAINTS;