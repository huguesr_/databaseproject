-- CREATE OR REPLACE PACKAGE initiator AS
--     PROCEDURE createTables;
--     PROCEDURE insertData;
--     PROCEDURE dropTables;
-- END initiator;
-- /
-- CREATE OR REPLACE PACKAGE BODY initiator AS
--     PROCEDURE createTables AS
--         BEGIN
--         EXECUTE IMMEDIATE 'Create Table Project_Location(
--             LocationID Number(10) GENERATED ALWAYS AS IDENTITY CONSTRAINT Location_PK PRIMARY KEY,
--             Address VARCHAR2(100) NOT NULL)';
        
--         EXECUTE IMMEDIATE 'Create Table Project_Warehouse(
--             WarehouseID Number(10) GENERATED ALWAYS AS IDENTITY CONSTRAINT Warehouse_PK PRIMARY KEY,
--             Warehousename VARCHAR2(100) NOT NULL,
--             LocationID Number(10) REFERENCES Project_Location (LocationID))';
        
--         EXECUTE IMMEDIATE 'Create Table Project_Products(
--             ProductID Number(10) GENERATED ALWAYS AS IDENTITY CONSTRAINT Product_PK PRIMARY KEY,
--             ProductName VARCHAR2(30) NOT NULL,
--             Category VARCHAR2(30) NOT NULL)';
        
--         EXECUTE IMMEDIATE 'Create Table Project_Warehouse_Products(
--             WarehouseID Number(10) REFERENCES Project_Warehouse (WarehouseID),
--             ProductID Number(10) REFERENCES Project_Products (ProductID),
--             Quantity Number(10) NOT NULL)';
        
--         EXECUTE IMMEDIATE 'Create Table Project_Store(
--             StoreID Number(10) GENERATED ALWAYS AS IDENTITY CONSTRAINT Store_PK PRIMARY KEY,
--             StoreName Varchar2(20) NOT NULL)';
        
--         EXECUTE IMMEDIATE 'Create Table Project_Store_Products(
--             StoreID Number(10) REFERENCES Project_Store (StoreID),
--             ProductID Number(10) REFERENCES Project_Products (ProductID),
--             Price Number(10) NOT NULL)';
        
--         EXECUTE IMMEDIATE 'Create Table Project_Review(
--             ReviewID Number(10) GENERATED ALWAYS AS IDENTITY CONSTRAINT Review_PK PRIMARY KEY,
--             Review Number(1) CHECK (Review <=5 OR Review IS NULL),
--             Flag Number(5) NULL,
--             Description Varchar2(300) NULL)';
        
--         EXECUTE IMMEDIATE 'Create Table Project_Customer(
--             CustID Number(10) GENERATED ALWAYS AS IDENTITY CONSTRAINT Customer_PK PRIMARY KEY,
--             FirstName Varchar2(20) NOT NULL,
--             LastName Varchar2(20) NOT NULL,
--             Email Varchar2(30) NOT NULL UNIQUE,
--             LocationID Number(10) REFERENCES Project_Location (LocationID))';
        
--         EXECUTE IMMEDIATE 'Create Table Project_Product_Review(
--             CustID Number(10) REFERENCES Project_Customer (CustID),
--             ProductID Number(10) REFERENCES Project_Products (ProductID),
--             ReviewID Number(10) REFERENCES Project_Review (ReviewID))';
        
--         EXECUTE IMMEDIATE 'Create Table Project_Orders(
--             OrderID Number(10) GENERATED ALWAYS AS IDENTITY CONSTRAINT Order_PK PRIMARY KEY,
--             OrderDate DATE NOT NULL,
--             StoreID Number(10) REFERENCES Project_Store (StoreID),
--             CustID Number(10) REFERENCES Project_Customer (CustID))';
        
--         EXECUTE IMMEDIATE 'Create Table Project_Customer_Products_Order(
--             ProductID Number(10),
--             OrderID Number(10),
--             Quantity Number(10) NOT NULL,
--             CONSTRAINT fk_product
--                 FOREIGN KEY (ProductID)
--                 REFERENCES Project_Products (ProductID),
--             CONSTRAINT fk_order
--                 FOREIGN KEY (OrderID)
--                 REFERENCES Project_Orders (OrderID)
--             )';
--     END;
    
--     PROCEDURE insertData AS
--         BEGIN
--         --1.Project_Customer Address
        
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Location (Address) --ID 1
--         VALUES (''dawson college, montreal, qeuebe, canada'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Location (Address) --ID 2
--         VALUES (''090 boul saint laurent, montreal, quebec, canada'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Location (Address) --ID 3
--         VALUES (''brossard, quebec, canada'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Location (Address) --ID 4
--         VALUES (''100 atwater street, toronto, canada'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Location (Address) --ID 5
--         VALUES (''boul saint laurent, montreal, quebec, canada'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Location (Address) --ID 6
--         VALUES (''100 young street, toronto, canada'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Location (Address) --ID 7
--         VALUES (''100 boul saint laurent, montreal, quebec, canada'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Location (Address) --ID 8
--         VALUES (''calgary, alberta, canada'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Location (Address) --ID 9
--         VALUES (''104 gill street, toronto, canada'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Location (Address) --ID 10
--         VALUES (''105 young street, toronto, canada'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Location (Address) --ID 11
--         VALUES (''87 boul saint laurent, montreal, quebec, canada'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Location (Address) --ID 12
--         VALUES (''76 boul decathlon, laval, quebec, canada'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Location (Address) --ID 13
--         VALUES (''22222 happy street, laval, quebec, canada'')';
        
        
--         --1.WareHouses Address
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Location (Address) --ID 14
--         VALUES (''100 rue William, saint laurent, Quebec, Canada'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Location (Address) --ID 15
--         VALUES (''304 Rue François-Perrault, Villera Saint-Michel, Montréal, QC'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Location (Address) --ID 16
--         VALUES (''86700 Weston Rd, Toronto, Canada'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Location (Address) --ID 17
--         VALUES (''170  Sideroad, Quebec City, Canada'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Location (Address) --ID 18
--         VALUES (''1231 Trudea road, Ottawa, Canada'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Location (Address) --ID 19
--         VALUES (''16  Whitlock Rd, Alberta, Canada'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Location (Address) --ID 20
--         VALUES (''homeless guy '')';
        
--         --2.Warehouses
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Warehouse (Warehousename,LocationID) --ID 1
--         VALUES (''Project_Warehouse A'',14)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Warehouse (Warehousename,LocationID) --ID 2
--         VALUES (''Project_Warehouse B'',15)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Warehouse (Warehousename,LocationID) --ID 3
--         VALUES (''Project_Warehouse C'',16)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Warehouse (Warehousename,LocationID) --ID 4
--         VALUES (''Project_Warehouse D'',17)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Warehouse (Warehousename,LocationID) --ID 5
--         VALUES (''Project_Warehouse E'',18)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Warehouse (Warehousename,LocationID) --ID 6
--         VALUES (''Project_Warehouse F'',19)';
        
--         --3.Product
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Products(ProductName,Category) --ID 1
--         VALUES (''laptop ASUS 104S','electronics'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Products(ProductName,Category) --ID 2
--         VALUES (''apple','Grocery'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Products(ProductName,Category) --ID 3
--         VALUES (''SIMS CD','Video Games'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Products(ProductName,Category) --ID 4
--         VALUES (''orange','Grocery'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Products(ProductName,Category) --ID 5
--         VALUES (''Barbie Movie','DVD'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Products(ProductName,Category) --ID 6
--         VALUES (''LOreal Normal Hair','Health'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Products(ProductName,Category) --ID 7
--         VALUES (''BMW iX Lego','Toys'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Products(ProductName,Category) --ID 8
--         VALUES (''BMW i6','Cars'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Products(ProductName,Category) --ID 9
--         VALUES (''Truck 500c','Vehicle'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Products(ProductName,Category) --ID 10
--         VALUES (''paper towel','Beauty'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Products(ProductName,Category) --ID 11
--         VALUES (''plum','grocery'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Products(ProductName,Category) --ID 12
--         VALUES (''Lamborghini Lego','Toys'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Products(ProductName,Category) --ID 13
--         VALUES (''chicken','grocery'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Products(ProductName,Category) --ID 14
--         VALUES (''pasta','Grocery'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Products(ProductName,Category) --ID 15
--         VALUES (''PS5','electronics'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Products(ProductName,Category) --ID 16
--         VALUES (''tomato','Grocery'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Products(ProductName,Category) --ID 18
--         VALUES (''Train X745','Vehicule'')';
        
--         --4.WareHouses Product
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
--         VALUES (1,1,1000)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
--         VALUES (1,7,10)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
--         VALUES (1,8,6)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
--         VALUES (1,14,2132)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
--         VALUES (1,16,352222)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
--         VALUES (2,2,24980)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
--         VALUES (2,16,39484)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
--         VALUES (3,3,103)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
--         VALUES (3,11,43242)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
--         VALUES (4,4,35405)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
--         VALUES (4,11,6579)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
--         VALUES (4,15,123)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
--         VALUES (5,5,40)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
--         VALUES (5,15,1000)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
--         VALUES (5,12,98765)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
--         VALUES (5,15,4543)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
--         VALUES (6,6,450)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
--         VALUES (6,10,3532)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Warehouse_Products(WarehouseID,ProductID,Quantity)
--         VALUES (6,13,43523)';
        
--         --5.Stores
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store(StoreName) --ID 1
--         VALUES (''marche adonis'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store(StoreName) --ID 2
--         VALUES (''marche atwater'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store(StoreName) --ID 3
--         VALUES (''dawson Project_Store'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store(StoreName) --ID 4
--         VALUES (''Project_Store magic'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store(StoreName) --ID 5
--         VALUES (''movie Project_Store'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store(StoreName) --ID 6
--         VALUES (''super rue champlain'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store(StoreName) --ID 7
--         VALUES (''toy r us'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store(StoreName) --ID 8
--         VALUES (''Dealer One'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store(StoreName) --ID 9
--         VALUES (''dealer montreal'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store(StoreName) --ID 10
--         VALUES (''movie start'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store(StoreName) --ID 11
--         VALUES (''star Project_Store'')';
        
--         --6.Stores-Product
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
--         VALUES (1,1,970)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
--         VALUES (2,2,10)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
--         VALUES (3,3,50)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
--         VALUES (4,4,2)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
--         VALUES (5,5,30)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
--         VALUES (6,6,10)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
--         VALUES (7,7,40)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
--         VALUES (8,8,50000)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
--         VALUES (9,9,856600)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
--         VALUES (10,10,50)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
--         VALUES (2,11,10)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
--         VALUES (6,6,30)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
--         VALUES (7,12,40)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
--         VALUES (7,12,80)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
--         VALUES (5,3,16)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
--         VALUES (7,5,45)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
--         VALUES (1,13,9.5)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
--         VALUES (2,14,13.5)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
--         VALUES (11,15,200)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
--         VALUES (7,7,38)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Store_Products (StoreID,ProductID,Price)
--         VALUES (4,14,15)';
        
--         --7.Reviews
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Review (Review,Flag,Description) --ID 1
--         VALUES (4,0,''it was affordable.'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Review (Review,Flag,Description) --ID 2
--         VALUES (3,0,''quality was not good'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Review (Review,Flag,Description) --ID 3
--         VALUES (2,1,NULL)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Review (Review,Flag,Description) --ID 4
--         VALUES (5,0,''highly recommend'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Review (Review,Flag,Description) --ID 5
--         VALUES (1,0,NULL)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Review (Review,Flag,Description) --ID 6
--         VALUES (1,0,''did not worth the price'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Review (Review,Flag,Description) --ID 7
--         VALUES (1,0,''missing some parts'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Review (Review,Flag,Description) --ID 8
--         VALUES (5,1,''trash'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Review (Review,Flag,Description) --ID 9
--         VALUES (2,0,NULL)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Review (Review,Flag,Description) --ID 10
--         VALUES (5,0,NULL)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Review (Review,Flag,Description) --ID 11
--         VALUES (4,0,NULL)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Review (Review,Flag,Description) --ID 12
--         VALUES (3,0,NULL)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Review (Review,Flag,Description) --ID 13
--         VALUES (1,0,''missing some parts'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Review (Review,Flag,Description) --ID 14
--         VALUES (4,NULL,NULL)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Review (Review,Flag,Description) --ID 15
--         VALUES (1,0,''great product'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Review (Review,Flag,Description) --ID 16
--         VALUES (5,1,''bad quality'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Review (Review,Flag,Description) --ID 17
--         VALUES (1,0,NULL)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Review (Review,Flag,Description) --ID 18
--         VALUES (4,0,NULL)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Review (Review,Flag,Description) --ID 19
--         VALUES (4,NULL,NULL)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Review (Review,Flag,Description) --ID 20
--         VALUES (5,NULL,NULL)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Review (Review,Flag,Description) --ID 21
--         VALUES (1,2,''worse car i have droven!'')';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Review (Review,Flag,Description) --ID 22
--         VALUES (4,NULL,NULL)';
        
--         --8.Customers
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer (FirstName,LastName,Email,LocationID) --ID 1
--         VALUES (''mahsa'',''sadeghi'',''msadeghi@dawsoncollege.qc.ca'',1)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer (FirstName,LastName,Email,LocationID) --ID 2
--         VALUES (''alex'',''brown'',''alex@gmail.com'',2)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer (FirstName,LastName,Email,LocationID) --ID 3
--         VALUES (''martin'',''alexandre'',''marting@yahoo.com'',3)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer (FirstName,LastName,Email,LocationID) --ID 4
--         VALUES (''daneil'',''hanne'',''daneil@yahoo.com'',4)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer (FirstName,LastName,Email,LocationID) --ID 5
--         VALUES (''John','boura','bdoura@gmail.com'',6)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer (FirstName,LastName,Email,LocationID) --ID 6
--         VALUES (''Ari'',''brown'',''b.a@gmail.com'',20)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer (FirstName,LastName,Email,LocationID) --ID 7
--         VALUES (''Amanda'',''Harry'',''am.harry@yahioo.com'',7)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer (FirstName,LastName,Email,LocationID) --ID 8
--         VALUES (''Jack'',''Jonhson'',''johnson.a@gmail.com'',8)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer (FirstName,LastName,Email,LocationID) --ID 9
--         VALUES (''John'',''belle'',''abcd@yahoo.com'',10)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer (FirstName,LastName,Email,LocationID) --ID 10
--         VALUES (''martin'',''Li'',''m.li@gmail.com'',11)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer (FirstName,LastName,Email,LocationID) --ID 11
--         VALUES (''olivia'',''smith'',''smith@hotmail.com'',12)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer (FirstName,LastName,Email,LocationID) --ID 12
--         VALUES (''Noah'',''Garcia'',''g.noah@yahoo.com'',13)';
        
--         --9.Product_Reviews
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
--         VALUES (1,1,1)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
--         VALUES (2,2,2)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
--         VALUES (3,3,3)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
--         VALUES (4,4,4)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
--         VALUES (2,5,5)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
--         VALUES (3,6,6)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
--         VALUES (1,7,7)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
--         VALUES (5,8,8)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
--         VALUES (6,9,9)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
--         VALUES (7,10,10)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
--         VALUES (8,11,11)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
--         VALUES (3,6,12)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
--         VALUES (1,12,13)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
--         VALUES (1,11,14)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
--         VALUES (1,12,15)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
--         VALUES (9,8,16)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
--         VALUES (2,3,17)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
--         VALUES (2,5,18)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
--         VALUES (10,13,19)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
--         VALUES (11,14,20)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
--         VALUES (1,7,21)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Product_Review(CustID,ProductID,ReviewID)
--         VALUES (12,14,22)';
        
--         --10.Orders
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 1
--         VALUES (TO_DATE(''21/4/2023'',''dd/mm/yyyy''),1,1)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 2
--         VALUES (TO_DATE(''23/10/2023'',''dd/mm/yyyy''),2,2)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 3
--         VALUES (TO_DATE(''1/10/2023'',''dd/mm/yyyy''),3,3)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 4
--         VALUES (TO_DATE(''23/10/2023'',''dd/mm/yyyy''),4,4)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 5
--         VALUES (TO_DATE(''23/10/2023'',''dd/mm/yyyy''),5,2)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 6
--         VALUES (TO_DATE(''10/10/2023'',''dd/mm/yyyy''),6,3)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 7
--         VALUES (TO_DATE(''11/10/2023'',''dd/mm/yyyy''),7,1)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 8
--         VALUES (TO_DATE(''10/10/2023'',''dd/mm/yyyy''),8,5)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 9
--         VALUES (TO_DATE(''6/5/2020'',''dd/mm/yyyy''),2,8)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 10
--         VALUES (TO_DATE(''12/9/2019'',''dd/mm/yyyy''),6,3)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 11
--         VALUES (TO_DATE(''11/10/2010'',''dd/mm/yyyy''),7,1)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 12
--         VALUES (TO_DATE(''6/5/2022'',''dd/mm/yyyy''),2,1)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 13
--         VALUES (TO_DATE(''7/10/2023'',''dd/mm/yyyy''),7,1)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 14
--         VALUES (TO_DATE(''10/8/2023'',''dd/mm/yyyy''),8,9)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 15
--         VALUES (TO_DATE(''23/10/2023'',''dd/mm/yyyy''),5,2)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 16
--         VALUES (TO_DATE(''2/10/2023'',''dd/mm/yyyy''),7,2)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 17
--         VALUES (TO_DATE(''3/4/2019'',''dd/mm/yyyy''),1,10)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 18
--         VALUES (TO_DATE(''29/12/2021'',''dd/mm/yyyy''),2,11)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 19
--         VALUES (TO_DATE(''20/1/2020'',''dd/mm/yyyy''),11,12)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 20
--         VALUES (TO_DATE(''11/10/2022'',''dd/mm/yyyy''),7,1)';
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Orders (OrderDate,StoreID,CustID) --ID 21
--         VALUES (TO_DATE(''29/12/2021'',''dd/mm/yyyy''),4,11)';
        
--         --11.Customer_Products_Order
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
--         VALUES (1,1,1)'; --laptop
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
--         VALUES (2,2,2)'; --apple
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
--         VALUES (3,3,3)'; --SIMS
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
--         VALUES (4,4,1)'; --orange
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
--         VALUES (5,5,1)'; --Barbie
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
--         VALUES (6,6,1)'; --Hair
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
--         VALUES (7,7,1)'; --BMW Lego
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
--         VALUES (8,8,1)'; --BMW car
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
--         VALUES (11,9,6)'; --plum
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
--         VALUES (6,10,3)'; --Hair
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
--         VALUES (12,11,1)'; --Lambo Lego
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
--         VALUES (11,12,7)'; --plum
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
--         VALUES (12,13,2)'; --lambo lego
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
--         VALUES (8,14,1)'; --BMW car
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
--         VALUES (3,15,1)'; --Sims
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
--         VALUES (5,16,1)'; --Barbie
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
--         VALUES (13,17,1)'; --chicken
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
--         VALUES (14,18,3)'; --Pasta
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
--         VALUES (15,19,1)'; --PS5
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
--         VALUES (7,20,1)'; --BMW lego
--         EXECUTE IMMEDIATE 'INSERT INTO Project_Customer_Products_Order (ProductID,OrderID,Quantity)
--         VALUES (14,21,3)'; --pasta
--     END;
    
--     PROCEDURE dropTables AS
--     BEGIN
--         EXECUTE IMMEDIATE 'DROP TABLE Project_Customer_Products_Order CASCADE CONSTRAINTS';
--         EXECUTE IMMEDIATE 'DROP TABLE Project_Orders CASCADE CONSTRAINTS';
--         EXECUTE IMMEDIATE 'DROP TABLE Project_Product_Review CASCADE CONSTRAINTS';
--         EXECUTE IMMEDIATE 'DROP TABLE Project_Customer CASCADE CONSTRAINTS';
--         EXECUTE IMMEDIATE 'DROP TABLE Project_Review CASCADE CONSTRAINTS';
--         EXECUTE IMMEDIATE 'DROP TABLE Project_Store_Products CASCADE CONSTRAINTS';
--         EXECUTE IMMEDIATE 'DROP TABLE Project_Store CASCADE CONSTRAINTS';
--         EXECUTE IMMEDIATE 'DROP TABLE Project_Warehouse_Products CASCADE CONSTRAINTS';
--         EXECUTE IMMEDIATE 'DROP TABLE Project_Products CASCADE CONSTRAINTS';
--         EXECUTE IMMEDIATE 'DROP TABLE Project_Warehouse CASCADE CONSTRAINTS';
--         EXECUTE IMMEDIATE 'DROP TABLE Project_Location CASCADE CONSTRAINTS';
--     END;
    
-- END initiator;

-- BEGIN
--     initiator.dropTables;
-- END;
    

Create Table Project_Location(
    LocationID Number(10) GENERATED ALWAYS AS IDENTITY CONSTRAINT Location_PK PRIMARY KEY,
    Address VARCHAR2(100) NOT NULL);

Create Table Project_Warehouse(
    WarehouseID Number(10) GENERATED ALWAYS AS IDENTITY CONSTRAINT Warehouse_PK PRIMARY KEY,
    Warehousename VARCHAR2(100) NOT NULL,
    LocationID Number(10) REFERENCES Project_Location (LocationID));

Create Table Project_Products(
    ProductID Number(10) GENERATED ALWAYS AS IDENTITY CONSTRAINT Product_PK PRIMARY KEY,
    ProductName VARCHAR2(30) NOT NULL,
    Category VARCHAR2(30) NOT NULL);

Create Table Project_Warehouse_Products(
    WarehouseID Number(10),
    ProductID Number(10),
    Quantity Number(10) NOT NULL,
    CONSTRAINT fk_wp_warehouseid
        FOREIGN KEY (WarehouseID)
        REFERENCES Project_Warehouse (WarehouseID),
    CONSTRAINT fk_wp_productid
        FOREIGN KEY (ProductID)
        REFERENCES Project_Products (ProductID));

Create Table Project_Store(
    StoreID Number(10) GENERATED ALWAYS AS IDENTITY CONSTRAINT Store_PK PRIMARY KEY,
    StoreName Varchar2(20) NOT NULL);

Create Table Project_Store_Products(
    StoreID Number(10),
    ProductID Number(10),
    Price Number(10) NOT NULL,
    CONSTRAINT fk_sp_storeid
        FOREIGN KEY (StoreID)
        REFERENCES Project_Store (StoreID),
    CONSTRAINT fk_sp_productid
        FOREIGN KEY (ProductID)
        REFERENCES Project_Products (ProductID));

Create Table Project_Review(
    ReviewID Number(10) GENERATED ALWAYS AS IDENTITY CONSTRAINT Review_PK PRIMARY KEY,
    Review Number(1) CHECK (Review <=5 OR Review IS NULL),
    Flag Number(5) NULL,
    Description Varchar2(300) NULL);

Create Table Project_Customer(
    CustID Number(10) GENERATED ALWAYS AS IDENTITY CONSTRAINT Customer_PK PRIMARY KEY,
    FirstName Varchar2(20) NOT NULL,
    LastName Varchar2(20) NOT NULL,
    Email Varchar2(50) NOT NULL UNIQUE,
    LocationID Number(10),
    CONSTRAINT fk_customer_locationid
        FOREIGN KEY (LocationID)
        REFERENCES Project_Location (LocationID));

Create Table Project_Product_Review(
    CustID Number(10),
    ProductID Number(10),
    ReviewID Number(10),
    CONSTRAINT fk_pr_custid
        FOREIGN KEY (CustID)
        REFERENCES Project_Customer (CustID),
    CONSTRAINT fk_pr_productid
        FOREIGN KEY (ProductID)
        REFERENCES Project_Products (ProductID),
    CONSTRAINT fk_pr_reviewid
        FOREIGN KEY (ReviewID)
        REFERENCES Project_Review (ReviewID));

Create Table Project_Orders(
    OrderID Number(10) GENERATED ALWAYS AS IDENTITY CONSTRAINT Order_PK PRIMARY KEY,
    OrderDate DATE NOT NULL,
    StoreID Number(10),
    CustID Number(10),
    CONSTRAINT fk_orders_store
        FOREIGN KEY (StoreID)
        REFERENCES Project_Store (StoreID),
    CONSTRAINT fk_orders_custid
        FOREIGN KEY (CustID)
        REFERENCES Project_Customer (CustID));

Create Table Project_Customer_Products_Order(
    ProductID Number(10),
    OrderID Number(10),
    Quantity Number(10) NOT NULL,
    CONSTRAINT fk_cpo_product
        FOREIGN KEY (ProductID)
        REFERENCES Project_Products (ProductID),
    CONSTRAINT fk_cpo_order
        FOREIGN KEY (OrderID)
        REFERENCES Project_Orders (OrderID)
    );