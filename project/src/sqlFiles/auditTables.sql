Create Table Audit_Location(
    Audit_LocationID Number(10) GENERATED ALWAYS AS IDENTITY CONSTRAINT Audit_Location_PK PRIMARY KEY,
    LocationID Number(10),
    Address VARCHAR2(100)  NULL,
    Old_Address VARCHAR2(100)  NULL,
    Action Varchar2(20) NOT NULL,
    Timestmp TimeStamp DEFAULT CURRENT_TIMESTAMP);

Create Table Audit_Warehouse(
    Audit_WarehouseID Number(10) GENERATED ALWAYS AS IDENTITY CONSTRAINT Audit_Warehouse_PK PRIMARY KEY,
    WareHouseID Number(10),
    Warehousename VARCHAR2(100) NULL, --drop tables and recreate
    Old_Warehousename VARCHAR2(100) NULL,
    LocationID Number(10),
    Action Varchar2(20) NOT NULL,
    Timestmp TimeStamp DEFAULT CURRENT_TIMESTAMP);

Create Table Audit_Products(
    Audit_ProductID Number(10) GENERATED ALWAYS AS IDENTITY CONSTRAINT Audit_Product_PK PRIMARY KEY,
    ProductID Number(10),
    ProductName VARCHAR2(30) NULL,
    Old_ProductName VARCHAR2(30) NULL,
    Category VARCHAR2(30)  NULL,
    Old_Category VARCHAR2(30) NULL,
    Action Varchar2(20) NOT NULL,
    Timestmp TimeStamp DEFAULT CURRENT_TIMESTAMP);

Create Table Audit_Warehouse_Products(
    WarehouseID Number(10),
    ProductID Number(10),
    Quantity Number(10) NULL,
    Old_Quantity Number(10) NULL,
    Action Varchar2(20) NOT NULL,
    Timestmp TimeStamp DEFAULT CURRENT_TIMESTAMP);

Create Table Audit_Store(
    Audit_StoreID Number(10) GENERATED ALWAYS AS IDENTITY CONSTRAINT Audit_Store_PK PRIMARY KEY,
    StoreID Number(10),
    StoreName Varchar2(20) NULL,
    OldStoreName Varchar2(20) NULL,
    Action Varchar2(20) NOT NULL,
    Timestmp TimeStamp DEFAULT CURRENT_TIMESTAMP);

Create Table Audit_Store_Products(
    StoreID Number(10),
    ProductID Number(10),
    Price Number(10) NULL,
    Old_Price Number(10) NULL,
    Action Varchar2(20) NOT NULL,
    Timestmp TimeStamp DEFAULT CURRENT_TIMESTAMP);

Create Table Audit_Review(
    Audit_ReviewID Number(10) GENERATED ALWAYS AS IDENTITY CONSTRAINT Audit_Review_PK PRIMARY KEY,
    ReviewID Number(10),
    Flag Number(5) NULL,
    Old_Flag Number(5) NULL,
    Description Varchar2(300) NULL,
    Old_Description Varchar2(300) NULL,
    Action Varchar2(20) NOT NULL,
    Timestmp TimeStamp DEFAULT CURRENT_TIMESTAMP);

Create Table Audit_Customer(
    Audit_CustID Number(10) GENERATED ALWAYS AS IDENTITY CONSTRAINT Audit_Customer_PK PRIMARY KEY,
    CustID Number(10),
    FirstName Varchar2(20) NULL,
    Old_FirstName Varchar2(20) NULL,
    LastName Varchar2(20) NULL,
    Old_LastName Varchar2(20) NULL,
    Email Varchar2(50) NULL,
    Old_Email Varchar2(50) NULL,
    LocationID Number(10),
    Action Varchar2(20) NOT NULL,
    Timestmp TimeStamp DEFAULT CURRENT_TIMESTAMP);

Create Table Audit_Product_Review(
    CustID Number(10),
    ProductID Number(10),
    ReviewID Number(10),
    Action Varchar2(20) NOT NULL,
    Timestmp TimeStamp DEFAULT CURRENT_TIMESTAMP);

Create Table Audit_Orders(
    Audit_OrderID Number(10) GENERATED ALWAYS AS IDENTITY CONSTRAINT Audit_Order_PK PRIMARY KEY,
    OrderID Number(10),
    OrderDate DATE NULL,
    Old_OrderDate DATE NULL,
    StoreID Number(10),
    CustID Number(10),
    Action Varchar2(20) NOT NULL,
    Timestmp TimeStamp DEFAULT CURRENT_TIMESTAMP);

Create Table Audit_Customer_Products_Order(
    ProductID Number(10),
    OrderID Number(10),
    Quantity Number(10) NULL,
    Old_Quantity Number(10) NULL,
    Action Varchar2(20) NOT NULL,
    Timestmp TimeStamp DEFAULT CURRENT_TIMESTAMP);
